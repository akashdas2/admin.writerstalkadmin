<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionModel extends Model
{
    protected $table = "product_questions";
    protected $fillable = ['id','uuid', 'product_id', 'question', 'question_image', 'question_text','correct_answer','updated_at', 'created_at'];
    protected $hidden = [];
}
