<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerModel extends Model
{
    protected $table = "product_question_answers";
    protected $fillable = ['id','uuid', 'question_id','Answer','answer_index','updated_at', 'created_at'];
    protected $hidden = [];
}
