<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TranslateText extends Model
{
    //
    protected $table = 'translate_text';
    protected $fillable = [  'uuid', 'from_text', 'language','to_text'];
}
