<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckUserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = \auth()->user();
        if ($user->status != 'active' ){
            if(Auth::check()){
                auth()->user()->tokens()->delete();
                $response = [
                    'success'=> false,
                    'message'=> "User is suspended by Amdin"
                ];
            }

            return response()->json($response);
        }
        return $next($request);
    }
}
