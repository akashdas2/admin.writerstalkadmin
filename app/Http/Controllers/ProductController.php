<?php

namespace App\Http\Controllers;

use App\AnswerModel;
use App\ProductModel;
use App\QuestionModel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Psy\Util\Json;


class ProductController extends Controller
{
public function add_product(){
    return view('Product.add_form');
}
public function get_products(){
    $products = ProductModel::query()->select('uuid','product_id','punishment_time','language')->get();
    if (sizeof($products) > 0){
        foreach ($products as $product){
            $questions_count = QuestionModel::query()->where('product_id',$product->uuid)->count();
            $product['questions'] = $questions_count;
        }
    }

    return view('Product.list_products', compact('products'));
}
public function delete_product($uuid = null){
    $product = ProductModel::query()->where('uuid',$uuid)->first();
    if ($product){
        $questions = QuestionModel::query()->where('product_id',$product->uuid)->get();
        foreach ($questions as $question){
            $answers = AnswerModel::query()->where('question_id',$question->uuid)->delete();
        }
        $questions_delete = QuestionModel::query()->where('product_id',$product->uuid)->delete();
        if ($questions_delete){
            $product->delete();
        }
        return redirect()->back()->withErrors(['Deleted']);
    }else{
        return redirect('dashboard');
    }
}
public function edit_product($uuid = null){

    $product_data = ProductModel::query()->where('uuid',$uuid)->first();

    $questions = QuestionModel::query()->where('product_id',$uuid)->get();
    if (sizeof($questions) > 0){
        foreach ($questions as $question) {
            $answers = AnswerModel::query()->where('question_id',$question->uuid)->get();
            $question['answers'] = $answers;
        }
        $product_data->questions = $questions;
    }
    return view('Product.add_form',compact('product_data'));
}

public function submit_product(Request $request){
    $p_id = $request->input('p_id');
    $product_id = $request->input('product_id');
    $language = $request->input('language');
    $punishment = $request->input('punishment');
    $compete_teams = $request->input('compete_teams');
    $extra_intro_message = $request->input('extra_intro_message');
    $last_screen_message = $request->input('last_screen_message');
    $display_hidden_value = $request->input('display_hidden_value');
    $show_total_points = $request->input('show_total_points');
    $story_board_text = $request->input('story_board_text');
    $guide_pdf = $request->input('guide_pdf');
    $goal_text = $request->input('goal_text');
    $goal_image = $request->file('goal_image');
    $email_subject = $request->input('email_subject');
    $email_body = $request->input('email_body');
    $story_board_image = $request->input('story_board_image');
    $question_data = $request->input('questions_data');
    $question_data =json_decode($question_data);

     $validated = $request->validate([
        'product_id' => 'required',
        'language' => 'required',
        'punishment' => 'required',
        'story_board_text' => 'required',
        'email_subject' => 'required',
        'email_body' => 'required',
        'questions_data' => 'required',
        'compete_teams'=>'required'
    ]);
    $is_product = ProductModel::query()->where('product_id', $product_id)
        ->where('language',$language)->first();

    if($is_product){
        $p_id = $is_product->id;

    }
    $prod_arr = [
        'uuid' => Str::uuid() ,
        'product_id' =>  $product_id,
        'punishment_time' =>  $punishment,
        'language' =>  $language,
        'story_board_text' =>  $story_board_text,
        'goal_text' =>  $goal_text,
        'email_subject' =>  $email_subject,
        'email_body' =>  $email_body,
        'compete_teams'=> $compete_teams,
        'extra_intro_message'=> $extra_intro_message,
        'last_screen_message'=> $last_screen_message,
        'display_hidden_value'=>$display_hidden_value,
        'show_total_points'=> $show_total_points,
        'product_type' => 'basic'
    ];
  if($story_board_image){
        $random_int = rand(100, 100000);
        $extension = $story_board_image->guessExtension();
        $file_name = "story_" . $random_int . "." . $extension;
        $story_board_image->move('uploads/story_board/', $file_name);
        $story_photo_path = 'uploads/story_board/'.$file_name;
        $prod_arr['story_board_image'] = $story_photo_path;

    }
    if ($guide_pdf){
        $random_int = rand(100, 100000);
        $extension = $guide_pdf->guessExtension();
        $file_name = "guide_" . $random_int . "." . $extension;
        $guide_pdf->move('uploads/guide_pdfs/', $file_name);
        $guide_pdf_path = 'uploads/guide_pdfs/' . $file_name;

        $prod_arr['guide_pdf'] = $guide_pdf_path;


    }
    if ($goal_image){
        //    Goal Image
        $random_int = rand(100, 100000);
        $extension_image = $goal_image->guessExtension();
        $file_name_image = "story_" . $random_int . "." . $extension_image;
        $goal_image->move('uploads/goal_image/', $file_name_image);
        $goal_photo_path = 'uploads/goal_image/'.$file_name_image;

        $prod_arr['goal_image'] = $goal_photo_path;


    }

    $add_product = ProductModel::updateOrCreate(['id'=>$p_id], $prod_arr);
    if ($add_product){
        if (sizeof($question_data) > 0){
            foreach ($question_data as $question){

                $random_int = rand(100,100000);
                if (strpos($question->question_image, 'base64') != false){
                    $replace = preg_replace('#^data:image/\w+;base64,#i', '', $question->question_image);
                    $image_decode = base64_decode($replace);
                    $extension = explode('/', mime_content_type($question->question_image))[1];

                    $image_name = "E_img" .$random_int. time() . "." . $extension;
                    $path = public_path() . "/uploads/question_photos/" . $image_name;

                    $image_DB_path = "uploads/question_photos/" . $image_name;

                    file_put_contents($path, $image_decode);
                    $ques_arr = [
                        'uuid' => base64_encode(rand(9,9999)) ,
                        'product_id' =>  $add_product['uuid'],
                        'question' =>  $question->question,
                        'question_image' =>  $image_DB_path,
                        'question_text' =>  $question->question_description,
                        'correct_answer' =>  strtoupper($question->correct_answer),
                        'product_type' => 'basic',
                    ];
                }else{
                    $ques_arr = [
                        'uuid' => base64_encode(rand(9,9999)) ,
                        'product_id' =>  $add_product['uuid'],
                        'question' =>  $question->question,
                        'question_text' =>  $question->question_description,
                        'correct_answer' =>  strtoupper($question->correct_answer),
                    ];
                }


                $question_add = QuestionModel::updateOrCreate(['id'=> $question->id],$ques_arr);

                if ($question_add){
                    $alphabet =   array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
                    foreach ($question->answers as $key => $answer){

                        $answers_add = AnswerModel::updateOrCreate(['id'=>$answer->id],[
                            'uuid' => base64_encode(rand(9,9999)) ,
                            'question_id' =>  $question_add['uuid'],
                            'Answer' =>  $answer->answer,
                            'answer_index' =>  strtoupper($alphabet[$key]),
                        ]);


                    }
                }
            }
        }
    }

    return redirect('get-products')->withErrors(['success','Done']);


}
public function index1()
{
   
        
     return view('Product.SuperAdminPro');
}
public function pro_submit(Request $request)
{
    $ProductModel = new ProductModel();
    $ProductModel->uuid = Str::uuid();
    $ProductModel->language = $request->input('language');
    $ProductModel->punishment_time = $request->input('punishment');
    $ProductModel->compete_teams = $request->input('compete_teams');
    $ProductModel->product_id = $request->input('product_id');
    $ProductModel->product_type = 'pro';
    $ProductModel->email_subject= $request->input('email_subject');
    $ProductModel->email_body = $request->input('email_body');
    $ProductModel->extra_intro_message ='1';
    $ProductModel->last_screen_message ='1';
    $ProductModel->display_hidden_value = '1';
    $ProductModel->show_total_points = '1';
    $ProductModel->save();
     $validated = $request->validate([
        'email_subject' => 'required',
        'email_body' => 'required',
        'product_id' => 'required',
        'language' => 'required',
        'punishment' => 'required',
        'compete_teams' => 'required',

    ]);


    return redirect('get-products')->withErrors(['success','Done']);

}
public function CopyProduct($id)
{

     $copy_data = ProductModel::query()->select('*')->where('uuid',$id)->first();

     $str = '1234';
       
     
     $copymodel = new ProductModel();
     $digits = 4;
     $inc =rand(pow(10, $digits-1), pow(10, $digits)-1);
     $copymodel->uuid = Str::uuid();
     $copymodel->product_id = $copy_data->product_id.$inc;
     $copymodel->punishment_time = $copy_data->punishment_time;
     $copymodel->product_type = $copy_data->product_type;
     $copymodel->compete_teams = $copy_data->compete_teams;
     $copymodel->language = $copy_data->language;
     $copymodel->extra_intro_message = $copy_data->extra_intro_message;
     $copymodel->last_screen_message = $copy_data->last_screen_message ;
     $copymodel->display_hidden_value = $copy_data->display_hidden_value;
     $copymodel->show_total_points = $copy_data->show_total_points;
     $copymodel->story_board_text = $copy_data->story_board_text;
     $copymodel->story_board_image = $copy_data->story_board_image;
     $copymodel->goal_text = $copy_data->goal_text;
     $copymodel->goal_image = $copy_data->goal_image;
     $copymodel->email_body = $copy_data->email_body;
     $copymodel->save();

     return redirect()->back();
}

}
