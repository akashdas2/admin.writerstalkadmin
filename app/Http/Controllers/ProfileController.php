<?php

namespace App\Http\Controllers;

use App\Models\Like;
use App\Models\Penpal;
use App\Models\User;
use App\Models\UserProfileView;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ProfileController extends Controller
{
    //
    public function user_profile($user_id = null){
        $auth_user = Auth::user();
        if (!$user_id){

        }else{

            if ($auth_user->uuid == $user_id){
                $is_user = User::query()->with('posts')
                    ->with('highlights')
                    ->with('stories')
                    ->with('likes')
                    ->where('uuid', $user_id)->first();
                if ($is_user){
                    if (sizeof(@$is_user->posts)> 0){
                        foreach (@$is_user->posts as $i => $i_row){
                            $like_exist =  Like::query()->where('user_id', $auth_user->uuid)
                                ->where('post_id', $i_row->uuid)->first();
                            $like_counts =  Like::query()->where('post_id', $i_row->uuid)->count();

                            $i_row['like_counts'] = $like_counts;
                            if ($like_exist){
                                $i_row['is_like'] = true;
                            }else{
                                $i_row['is_like'] = false;
                            }
                        }
                    }

                    $penpals = Penpal::query()->where('status','Accept')->whereIn('sender_id',[$auth_user->uuid,$user_id])
                        ->whereIn('receiver_id',[$auth_user->uuid,$user_id])->get();
                    if (sizeof($penpals)>0){
                        foreach ($penpals as $p => $p_row){
                            $toPick = $auth_user->uuid;
                            if ($toPick == $p_row->sender_id){
                                $toPick = $p_row->receiver_id;
                            }else{
                                $toPick = $p_row->sender_id;
                            }
                            $penpal_user = User::query()->where('uuid', $toPick)->first();
                            $penpals_user_count = Penpal::query()->where('status','Accept')->where('sender_id',$toPick)
                                ->orWhere('receiver_id',$toPick)->count();
                            $penpal_user['penpal_user_count'] = $penpals_user_count;
                            $p_row['penpal_user'] = $penpal_user;
                        }
                    }
                    $is_user['penpals_count'] = count($penpals);
                    $is_user['penpals'] = $penpals;
                    return view('profile',compact('is_user'));

                }
            }
        }
        return view('profile');
    }
    public function user_profile_data(Request $request){

        $user = Auth::user();
        $user_id = $request->input('user_id');
        $validator = Validator::make($request->all(), [
            'user_id'=>'required',

        ]);
        $final_data = [];
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        if ($user->uuid == $user_id){
            $is_user = User::query()->with('posts')
                ->with('highlights')
                ->with('stories')
                ->with('likes')
                ->where('uuid', $user_id)->first();
            if ($is_user){

                $penpals = Penpal::query()->where('status','Accept')->where('sender_id',$user_id)
                    ->orWhere('receiver_id',$user_id)->get();
                if (sizeof($penpals)>0){
                    foreach ($penpals as $p => $p_row){
                        $toPick = $user->uuid;
                        if ($toPick == $p_row->sender_id){
                            $toPick = $p_row->receiver_id;
                        }else{
                            $toPick = $p_row->sender_id;
                        }
                        $penpal_user = User::query()->where('uuid', $toPick)->first();
                        $penpals_user_count = Penpal::query()->where('status','Accept')->where('sender_id',$toPick)
                            ->orWhere('receiver_id',$toPick)->count();
                        $penpal_user['penpal_user_count'] = $penpals_user_count;
                        $p_row['penpal_user'] = $penpal_user;
                    }
                }
                $is_user['penpals_count'] = count($penpals);
                $is_user['penpals'] = $penpals;
                $resposne = [
                    'success'=> true,
                    'message'=> 'Record Found Success fully',
                    'data'=> $is_user,
                    'status'=>'profile'
                ];
            }
        }else{

            $profile_view_exist = UserProfileView::query()->where('user_id', $user_id)->first();
            if (!$profile_view_exist){

                $add_profile_view = UserProfileView::create([
                    'uuid'=>Str::uuid(),
                    'ip_address'=>$request->ip(),
                    'user_id'=>$user_id,
                    'agent'=> $request->header('user-agent'),
                ]);
                if ($add_profile_view){
                    $user_to_update = User::query()->where('uuid', $user_id)->first();
                    $update_view = $user_to_update->views + 1;
                    $user_to_update->update([
                        'views'=>$update_view
                    ]);
                }
            }

            $is_penpal = Penpal::query()
                ->whereIn('sender_id', [$user->uuid, $user_id])
                ->whereIn('receiver_id', [$user->uuid, $user_id])
                ->first();

            if ($is_penpal){
                if ($is_penpal->status == 'Accept'){

                    $is_user = User::query()->where('uuid', $user_id)
                        ->with('posts')
                        ->with('highlights')
                        ->with('stories')
                        ->with('likes')
                        ->first();
                    if ($is_user){
                        $penpal_count = Penpal::query()->where('status', 'Accept')->where('sender_id',$user_id)
                            ->orWhere('receiver_id', $user_id)->count();
                        $is_user['penpals_count'] = $penpal_count;
                    }
                    $resposne = [
                        'success'=> true,
                        'message'=> 'Record Found Success fully',
                        'data'=> $is_user,
                        'status' => 'Friends'
                    ];
                }else{
                    $is_user = User::query()->where('uuid', $user_id)
                        ->first();
                    if ($is_user){
                        $penpal_count = Penpal::query()->where('status', 'Accept')->where('sender_id',$user_id)
                            ->orWhere('receiver_id', $user_id)->count();
                        $is_user['penpals_count'] = $penpal_count;
                    }
                    $status = 'Request Sent';
                    if ($is_penpal->receiver_id == $user->uuid) {
                        $status = 'Accept';
                    }
                    $status_button = $status;
                    $resposne = [
                        'success'=> true,
                        'message'=> 'Record Found Success fully',
                        'data'=> $is_user,
                        'status'=>$status_button
                    ];
                }
            }else{
                $is_user = User::query()->where('uuid', $user_id)
                    ->first();
                if ($is_user){
                    $penpal_count = Penpal::query()->where('status', 'Accept')->where('sender_id',$user_id)
                        ->orWhere('receiver_id', $user_id)->count();
                    $is_user['penpals_count'] = $penpal_count;
                }
                $resposne = [
                    'success'=>true,
                    'message'=>'Penpal not found',
                    'data'=>$is_user,
                    'status'=>'Add Friend'
                ];
            }
        }
        return $resposne;
    }

    public function edit_user_profile(){
        return view('edit_profile');
    }

    public function get_explore_user_stories(){
        return view('explore_stories');
    }
    public function get_user_chats(){
        return view('chat');
    }
    public function get_user_penpals(){
        return view('penpals');
    }

    public function get_change_user_password(){
        return view('change_password');
    }
}
