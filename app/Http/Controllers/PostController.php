<?php

namespace App\Http\Controllers;

use App\Models\Like;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PostController extends Controller
{
    //

    public function submit_post(Request $request){
        $user = Auth::user();
        $image = $request->file('image');
        $video = $request->file('video');
        $description = $request->input('description');
        if (!$description){
            $description = '';
        }
        if ($image || $video  || $description) {

            $post_arr = [
                'uuid' => Str::uuid(),
                'user_id' => $user->uuid,
                'description' => $description,
                'suspend' => 0,
                'post_type'=>'user'
            ];

            if ($image || $video){


                if ($video){
                    $fileName = 'post_'.rand(999,9999).time() .'.'. strtolower($video->getClientOriginalExtension());
//                        $filePath = $file->move(public_path('uploads/videos'), $fileName);
                    $filePath = $video->storeAs('uploads/videos', $fileName, 'public');
                    $post_arr['file_type'] = 'video';

                }
                if ($image){
                    $fileName = 'post_'.rand(999,9999).time() .'.'. strtolower($image->getClientOriginalExtension());
//                        $filePath = $file->move(public_path('uploads/images'), $fileName);
                    $filePath = $image->storeAs('uploads/images', $fileName, 'public');
                    $post_arr['file_type'] = 'image';

                }


                $post_arr['file'] = $filePath;

            }
            $add_post  = Post::create($post_arr);

            if ($add_post){
                return redirect()->back()->with('success','Post Added Successfully');
            }else{
                return redirect()->back()->with('error','Failed to Add Post');

            }

        }else{
            return redirect()->back()->with('error','File or Description is requried to add post ');

        }


    }

    public function save_post_like(Request $request){
        $uuid = $request->input('uuid');
        $post_type = $request->input('post_type');
        $auth_user = Auth::user();
        if($auth_user){
           $is_post =  Post::query()->where('uuid', $uuid)->first();
           if ($is_post){
              $is_like = Like::query()->where('post_id',$is_post->uuid)
                            ->where('user_id', $auth_user->uuid)
                            ->where('post_type', $post_type)->first();
              if ($is_like){
                  $is_like->delete();
                  $like_count =Like::query()->where('post_id',$is_post->uuid)->count();
                  $response = [
                      'success'=> true,
                      'target'=>'unlike',
                      'like_count'=>$like_count,
                  ];
              }else{
                  Like::create([
                      'uuid'=> Str::uuid(),
                      'user_id'=>$auth_user->uuid,
                      'post_id'=>$is_post->uuid,
                      'post_type'=> $post_type
                  ]);
                  $like_count =Like::query()->where('post_id',$is_post->uuid)->count();
                  $response = [
                      'success'=> true,
                      'target'=>'like',
                      'like_count'=>$like_count,
                  ];
              }
           }
            return json_encode($response);
        }else{
            return redirect('login');
        }
        return json_encode($response);

    }


}
