<?php

namespace App\Http\Controllers;

use App\ManagerSetupModel;
use Illuminate\Http\Request;

class EventController extends Controller
{

   public function event_manager(){
       $manager_screen_data = ManagerSetupModel::query()->first();
       return view('Event.manage_event', compact('manager_screen_data'));
   }
   public function submit_manager_setup(Request $request){
       $language = $request->input('language');
       $image = $request->file('image');
       $text_after_login = $request->input('text_after_login');
       $create_new_event_text = $request->input('create_new_event_text');
       $activation_code_text = $request->input('activation_code_text');
       $phone_number_text = $request->input('phone_number_text');
       $how_to_pin_map_text = $request->input('how_to_pin_map_text');
       $overview_text = $request->input('overview_text');
       $final_result_text = $request->input('final_result_text');
       $validated = $request->validate([
           'language' => 'required',
           'text_after_login' => 'required',
           'create_new_event_text' => 'required',
           'activation_code_text' => 'required',
           'phone_number_text' => 'required',
           'how_to_pin_map_text' => 'required',
           'overview_text' => 'required',
           'final_result_text' => 'required',
       ]);


       if($image){
           $random_int = rand(100, 100000);
           $extension = $image->guessExtension();
           $file_name = "manager_" . $random_int . "." . $extension;
           $image->move('uploads/manager_setup/', $file_name);
           $manager_photo_path = 'uploads/manager_setup/' . $file_name;
           $data_arr = [
               'uuid' => base64_encode(rand(9,9999)) ,
               'language' => $language,
               'text_after_login' => $text_after_login,
               'create_new_event_text' => $create_new_event_text,
               'activation_code_text' => $activation_code_text,
               'phone_number_text' => $phone_number_text,
               'how_to_pin_map_text' => $how_to_pin_map_text,
               'overview_text' => $overview_text,
               'final_result_text' => $final_result_text,
               'image' => $manager_photo_path,
           ];
       }else{
           $data_arr = [
               'uuid' => base64_encode(rand(9,9999)) ,
               'language' => $language,
               'text_after_login' => $text_after_login,
               'create_new_event_text' => $create_new_event_text,
               'activation_code_text' => $activation_code_text,
               'phone_number_text' => $phone_number_text,
               'how_to_pin_map_text' => $how_to_pin_map_text,
               'overview_text' => $overview_text,
               'final_result_text' => $final_result_text,

           ];
       }


       $add_or_create_setup = ManagerSetupModel::updateOrCreate(['id'=>'1'],$data_arr

           );
if ($add_or_create_setup){
    return redirect()->back()->withErrors(['success' => "Record added"]);
}
//       return view('Event.manage_event');
   }
}
