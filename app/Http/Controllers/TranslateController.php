<?php

namespace App\Http\Controllers;

use App\TranslateText;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TranslateController extends Controller
{
    //

    public function get_translated_text(){
        $data['text_lists'] = TranslateText::orderBy('id', 'desc')->paginate(10);
        return view('Translate.index', $data);
    }

    public function create_translate_text(Request $request){

        return view('Translate.add_form');

    }

    public function submit_translate_text(Request $request){

            $t_uuid = $request->input('text_uuid');
            $text_from = $request->input('text_from');
            $text_to = $request->input('text_to');
            $text_language = $request->input('from_text_language');

        $validated = $request->validate([
            'text_from' => 'required',
            'text_to' => 'required',
            'from_text_language'=>'required'

        ]);
            $text_arr = [
                'uuid' => Str::uuid(),
                'from_text'=>$text_from,
                'to_text'=>$text_to,
                'language'=>$text_language,
            ];

            $add_text = TranslateText::updateOrCreate(['uuid'=>$t_uuid], $text_arr);

            if ($add_text){
                return redirect('/get-translated-text')->withErrors(['success','Done']);
            }


    }

    public function edit_translated_text($uuid = null){
        $text_data = TranslateText::query()->where('uuid',$uuid)->first();
        return view('Translate.add_form',compact('text_data'));


    }

    public function delete_translate_text($uuid = null){

        $text = TranslateText::query()->where('uuid',$uuid)->delete();
        return redirect()->back()->withErrors(['Deleted']);
    }
}
