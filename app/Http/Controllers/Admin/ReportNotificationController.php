<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Report;

class ReportNotificationController extends Controller
{
    public function index()
    {
        $reports = Report::get();
        return view('admin.report-notifications.index', compact('reports'));
    }
}
