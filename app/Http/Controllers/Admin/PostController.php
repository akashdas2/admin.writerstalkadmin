<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminPost;
use App\Models\AdminPostTag;
use App\Models\AdminTip;
use App\Models\Tag;
use App\Models\Video;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PostController extends Controller
{
    public function index() {
        $user =  Auth::guard('admin')->user();
        $admin_posts =AdminPost::query()->where('user_id',$user->uuid)->orderBy('id', 'DESC')->get();
        $tags = Tag::query()->orderByDesc('id')->get();
        return view('admin.posts.index', compact('admin_posts','tags'));
    }
    //
    public function create(){
        $tags = Tag::query()->orderByDesc('id')->get();
        return view('admin.posts.create', compact('tags'));
    }

    public function store(Request $request){
        $request->validate([
            'file' => 'required|mimes:jpg,pdf,jpeg,gif,png,csv,txt,xlx,xls,pdf,mp4',
            'title' => 'required',
            'description' => 'required',
            'tip_type' => 'required'
        ]);
        $title = $request->input('title');
        $description = $request->input('description');
        $type = $request->input('tip_type');
        $extension = $request->input('file_extension');
        $tags = $request->input('post_tags');
        $published_at = $request->input('published_at');
        $fileName = '';
        $thumbnailName = '';

        if($request->hasFile('file')) {
            $fileName = Str::random(6).time() . '.' . $request->file->getClientOriginalExtension();
            if(!Storage::disk('public')->exists("uploads/videos/$thumbnailName")) {
                Storage::disk('public')->makeDirectory('uploads/videos/');
            }
            $request->file('file')->storeAs('uploads/videos/', $fileName, 'public');
        }
        
        if($request->hasFile('thumbnail')) {
            $thumbnailName = Str::random(6).time() . '.' . $request->file('thumbnail')->getClientOriginalExtension();
            if(!Storage::disk('public')->exists("uploads/videos/thumbnails/$thumbnailName")) {
                Storage::disk('public')->makeDirectory('uploads/videos/thumbnails/');
            }
            $request->file('thumbnail')->storeAs('uploads/videos/thumbnails/', $thumbnailName, 'public');
        }

        $admin_post = AdminPost::create([
            'uuid'=>Str::uuid(),
            'user_id' => Auth::guard('admin')->user()->uuid,
            'tip_type'=> $type,
            'file'=> 'uploads/videos/'.$fileName,
            'thumbnail'=> 'uploads/videos/thumbnails/'.$thumbnailName,
            'file_type'=> $extension,
            'description'=> $description,
            'title'=> $title,
            'published_at' => empty($published_at) 
                ? date('Y-m-d H:i:s') 
                : date('Y-m-d H:i:s', strtotime($published_at))
        ]);
        if ($admin_post){
            if ($tags && sizeof($tags) > 0){
                foreach ($tags as $t=>$t_row){
                    $tag = Tag::query()->where('uuid', $t_row)->first();
                    if ($tag){
                      $admin_post_tag = AdminPostTag::create([
                           'uuid'=>Str::uuid(),
                           'post_id'=>$admin_post->uuid,
                           'tag_id'=>$tag->uuid
                       ]);

                    }
                }
            }
        }

        return back()
            ->with('success','File has been uploaded.')
            ->with('file', $fileName);

    }
    
    public function edit($uuid){
        $admin_post = AdminPost::where('uuid', $uuid)->first();
        $post_tags = AdminPostTag::where('post_id', $uuid)->get()->pluck('tag_id');
        $admin_post->post_tags = $post_tags;
        $tags = Tag::query()->orderByDesc('id')->get();
        return view('admin.posts.edit', compact('tags', 'admin_post'));
    }
    
    public function update(Request $request, $uuid){
        $admin_post = AdminPost::where('uuid', $uuid)->first();
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'tip_type' => 'required'
        ]);
        $title = $request->input('title');
        $description = $request->input('description');
        $type = $request->input('tip_type');
        $extension = $request->input('file_extension');
        $tags = $request->input('post_tags');
        $published_at = $request->input('published_at');
        $fileName = '';
        $thumbnailName = '';

        if($request->hasFile('file')) {
            $fileName = Str::random(6).time() . '.' . $request->file->getClientOriginalExtension();
            if(Storage::disk('public')->exists($admin_post->file)) {
                Storage::disk('public')->delete($admin_post->file);
            }
            $request->file('file')->storeAs('uploads/videos/', $fileName, 'public');
        }
        
        if($request->hasFile('thumbnail')) {
            $thumbnailName = Str::random(6).time() . '.' . $request->file('thumbnail')->getClientOriginalExtension();
            if(Storage::disk('public')->exists($admin_post->thumbnail)) {
                Storage::disk('public')->delete($admin_post->thumbnail);
            }
            $request->file('thumbnail')->storeAs('uploads/videos/thumbnails/', $thumbnailName, 'public');
        }
        if($fileName != '') {
            $admin_post->file = 'uploads/videos/'.$fileName;
            $admin_post->file_type = $extension;
        }
        if($thumbnailName != '') {
            $admin_post->thumbnail = 'uploads/videos/thumbnails/'.$thumbnailName;
        }
        $admin_post->tip_type = $type;
        $admin_post->title = $title;
        $admin_post->description = $description;
        $admin_post->published_at = empty($published_at) 
            ? date('Y-m-d H:i:s') 
            : date('Y-m-d H:i:s', strtotime($published_at));
        
        if ($admin_post->save()){
            if ($tags && sizeof($tags) > 0){
                $admin_post->tags()->delete();
                foreach ($tags as $t=>$t_row){
                    $tag = Tag::query()->where('uuid', $t_row)->first();
                    if ($tag){
                      $admin_post_tag = AdminPostTag::create([
                           'uuid'=>Str::uuid(),
                           'post_id'=>$admin_post->uuid,
                           'tag_id'=>$tag->uuid
                       ]);

                    }
                }
            }
        }

        return back()
            ->with('success','File has been uploaded.')
            ->with('file', $fileName);

    }

    public function destroy($uuid)
    {
        $admin_post = AdminPost::where('uuid', $uuid)->first();

        if(Storage::disk('public')->exists($admin_post->file)) {
            Storage::disk('public')->delete($admin_post->file);
        }

        if(Storage::disk('public')->exists($admin_post->thumbnail)) {
            Storage::disk('public')->delete($admin_post->thumbnail);
        }

        AdminPostTag::where('post_id', $uuid)->delete();
        $admin_post->delete();
        redirect()->route('admin.posts.index')->with('success', 'Post deleted successfully!');
    }
}
