<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminPostTag;
use App\Models\Genres;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class GenreController extends Controller
{
    public function index(){
        $genres = Genres::query()->orderByDesc('id')->get();
        return view('admin.genres',compact('genres'));
    }

    public function store(Request $request){
        $genres = $request->input('genres');
        $add_tag = Genres::create([
            'uuid'=>Str::uuid(),
            'genres'=>$genres
        ]);

        return redirect('/admin/genres')->with('success','Genre added Successfully.');

    }

    public function destroy($uuid = null){
        $genre = Genres::query()->where('uuid',$uuid)->first();
        if ($genre){
            $genre->delete();
        }
        return redirect('/admin/genres')->with('success','Genre deleted Successfully.');
    }
}
