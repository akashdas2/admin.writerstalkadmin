<?php

namespace App\Http\Controllers\Admin;

use App\Models\Quick;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PromptController
{
    public function __construct()
    {
    }

    public function index(){
        $prompts = Quick::all();
        if(sizeof($prompts) > 0) {
            foreach($prompts as $qck) {
                $quickData = User::query()->where('uuid', $qck->user_id)->first();
                $qck->user_details=$quickData;
            }
        }
        return view('admin.users.prompts',compact('prompts'));
    }

    public function store(Request $request)
    {
        $id = $request->get('id');
    }

    public function destroy($id): \Illuminate\Http\RedirectResponse
    {
        $prompt = Quick::query()->where('uuid', $id)->first();

        if ($prompt) {
            $url = config()->get('app.front_url');
            Http::post($url . '/api/prompt-destroy', [
                'uuid' => $prompt->uuid
            ]);
        }

        return redirect()->back()->with(['success' => 'Prompt deleted successfully']);
    }

}