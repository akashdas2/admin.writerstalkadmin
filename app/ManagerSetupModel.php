<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManagerSetupModel extends Model
{
    protected $table = "manager_setup";
    protected $fillable = ['id','uuid', 'language','image','text_after_login', 'final_result_text','create_new_event_text','activation_code_text','phone_number_text','how_to_pin_map_text','overview_text','updated_at', 'created_at'];
    protected $hidden = [];
}
