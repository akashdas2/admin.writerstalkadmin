<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    protected $table = "products";
    protected $fillable = ['id','uuid', 'guide_pdf' ,'product_id','activation_code_text','punishment_time','product_type','language',
    'story_board_text','story_board_image','compete_teams','extra_intro_message','show_total_points','last_screen_message','display_hidden_value','goal_text','goal_image','email_subject','email_body','updated_at', 'created_at'];
    protected $hidden = [];
}
