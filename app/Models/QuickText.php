<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuickText extends Model
{
    use HasFactory;
    protected $table = 'quick_texts';
    protected $fillable = ['id','uuid','user_id','text','updated_at','created_at'];
}
