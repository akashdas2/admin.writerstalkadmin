<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'owner_id',
        'reporter_id',
        'reportable_id',
        'reportable_type',
        'status',
        'seen_at',
        'created_at'
    ];


    public function reportable()
    {
        return $this->morphTo(__FUNCTION__, 'reportable_type', 'reportable_id', 'uuid');
    }


    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id', 'id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'owner_id', 'uuid');
    }

    public function reporter()
    {
        return $this->belongsTo(User::class, 'reporter_id', 'id');
    }
}
