@extends('layouts.main')

@section('title')
Profile
@endsection
@push('style')
    <style>
        .show-read-more .more-text{
            display: none;
        }
    </style>
@endpush
@section('content')
    <div class="w-2">
    </div>
    <!-- ***** User profile Starts ***** -->
    <div class="w-56">
        <div class="card w-100 border-0 bg shadow-xss rounded-xxl">
            <div class="card-body rounded-xxl"><img src="{{asset('assets/imgs/flower1.jpg')}}" class="bg-img" alt="image"></div>
            <div class="card-body position-relative pt-0">
                <figure class="avatar position-absolute profile w100"><img src="{{asset($is_user->image)}}" alt="image" class=" p-1 bg-white rounded-circle w-100"></figure>
                <div class="row">
                    <div class="col-8">
                        <h4 class=" fw-700 font-sm pl-6">{{@$is_user->name}}<span class="fw-500 font-xssss text-grey-500 mb-3 d-block">{{@$is_user->email}}</span></h4>
                    </div>
                    <div class="col-4">
                        <div class="d-flex align-items-center mb-3 ">
                            @if($is_user->uuid != \Illuminate\Support\Facades\Auth::user()->uuid)
                            <a href="#" class="bg-success pt-0 mt-0 p-3 rounded-3 text-white font-xsssss fw-700 ls-3">
                                ADD FRIEND
                            </a>
                            <a href="#" class="bg-greylight btn-round-lg ms-2 rounded-3 text-grey-700"><i class="far fa-envelope font-md"></i></a>
                            <a href="#" class="bg-greylight btn-round-lg ms-2 rounded-3 text-grey-700"><i class="fas fa-ellipsis-h font-md tetx-dark"></i></a>
                        @endif
                        </div>

                    </div>
                </div>
            </div>
{{--            <div class="card-body d-block w-100 shadow-none mb-0 p-0 border-top-xs">--}}
{{--                <ul class="nav nav-tabs h55 d-flex border-bottom-0 ps-4">--}}
{{--                    <li class="active list-inline-item me-5"><a class="fw-700 font-xssss text-grey-500 pt-3 pb-3 ls-1 d-inline-block active">About</a></li>--}}
{{--                    <li class="list-inline-item me-5"><a class="fw-700 font-xssss text-grey-500 pt-3 pb-3 ls-1 d-inline-block">Membership</a></li>--}}
{{--                </ul>--}}
{{--            </div>--}}
        </div>
        <!-- ***** user profile ends ***** -->

        <!-- ***** About us Starts ***** -->
        <div class="row mt-3">
            <div class="col-4 pr-0">
                <div class="card shadow-xss rounded-xxl border-0 mb-3">
                    <div class="card-body d-block p-3">
                        <h4 class="fw-700 mb-3 font-xsss text-grey-900">About</h4>
                        <p class="fw-500 text-grey-500 lh-24 font-xssss mb-0">
                            @if($is_user->bio)
                                {{@$is_user->bio}}
                            @else
                                No Bio
                            @endif
                        </p>
                    </div>
                    <div class="card-body border-top-xs d-flex font-size">
                        <i class="fas fa-lock text-grey-500 me-3"></i>
                        <h4 class="fw-700 text-grey-900 font-xssss mt-0">Favourite Genres
                            <span class="d-block font-xssss fw-500 mt-1 lh-3 text-grey-500">
                                @if(@$is_user->favorite_genres)
                                {{@$is_user->favorite_genres}}
                                @else
                                No Genres Selected
                                @endif
                            </span>
                        </h4>
                    </div>

{{--                    <div class="card-body d-flex pt-0">--}}
{{--                        <i class="far fa-eye text-grey-500 me-3 font-size"></i>--}}
{{--                        <h4 class="fw-700 text-grey-900 font-xssss mt-0">Visble <span class="d-block font-xssss fw-500 mt-1 lh-3 text-grey-500">Anyone can find you</span></h4>--}}
{{--                    </div>--}}
{{--                    <div class="card-body d-flex pt-0">--}}
{{--                        <i class="fas fa-map-marker-alt text-grey-500 me-3 font-size"></i>--}}
{{--                        <h4 class="fw-700 text-grey-900 font-xssss mt-1">Flodia, Austia </h4>--}}
{{--                    </div>--}}
{{--                    <div class="card-body d-flex pt-0">--}}
{{--                        <i class="fas fa-users text-grey-500 me-3 font-size"></i>--}}
{{--                        <h4 class="fw-700 text-grey-900 font-xssss mt-1">Genarel Group</h4>--}}
{{--                    </div>--}}
                </div>
            </div>
            <!-- ***** About us ends ***** -->

            <div class="col-8 w-100 ml-0">
                <div class="card w-100 shadow-xss rounded-xxl border-0 ps-4 pt-3 pe-4 pb-3">
                    <div class="card-body p-0 mb-2 mt-0 position-relative">
                        <figure class="avatar position-absolute ml-2 mt-2 top-5"><img src="{{asset($is_user->image)}}" alt="image" class="shadow-sm rounded-circle w30"></figure>
                        <textarea name="message" class="h100 bor-0 mt-1 w-100 rounded-xxl p-2 pl-5 font-xssss text-grey-500 fw-500 border-light-md theme-dark-bg" cols="30" rows="10" placeholder="What's on your mind?"></textarea>
                    </div>
                    <div class="card-body d-flex p-0 mt-0 mb-4">
                        <a href="#" class="d-flex align-items-center font-xssss fw-600 ls-1 text-grey-700 text-dark pe-4" ><i class="fas fa-video feather-video font-md text-danger me-2"></i><span class="d-none-xs">Live Video</span></a>
                        <a href="#" class="d-flex align-items-center font-xssss fw-600 ls-1 text-grey-700 text-dark pe-4"><i class="far fa-file-image font-md text-success feather-image me-2"></i><span class="d-none-xs">Photo/Video</span></a>
                    </div>
                </div>
                @if(sizeof(@$is_user->posts) > 0)
                   <input type="hidden" name="user_posts" id="userPostsArr" value="{{json_encode($is_user->posts)}}">
                   <input type="hidden" name="user_id" id="userId" value="{{\Illuminate\Support\Facades\Auth::user()->uuid}}">
                    @foreach(@$is_user->posts as $p => $p_row)
                    <div class="card w-100 shadow-xss rounded-xxl border-0 p-4 mb-0 mt-2">
                        <div class="card-body p-0 d-flex">
                            <figure class="avatar me-3 m-0"><img src="{{asset(@$is_user->image)}}" alt="image" class="shadow-sm rounded-circle w45"></figure>
                            <h4 class="fw-700 text-grey-900 font-xssss mt-1 ml-2">{{@$is_user->name}} <span class="d-block font-xssss fw-500 mt-1 lh-3 text-grey-500">2 hour ago</span></h4>
    {{--                        <a href="#" class="ms-auto"><i class="fas fa-ellipsis-h text-grey-900 btn-round-md bg-greylight font-xss"></i></a>--}}
                        </div>
                        <div class="card-body p-0 me-lg-5">
                            <p class="fw-500 text-grey-500 lh-26 font-xssss w-100 show-read-more">
                                @if(@$p_row->description)
                                {{@$p_row->description}}
                                @endif
                                @if(@$p_row->file_type == 'image')

                                        <img src="{{asset('storage/'.$p_row->file)}}" class="float-right w-100" alt="">
                                @endif
                                @if(@$p_row->file_type == 'video')
                                        <video autoplay="" loop="" class="float-right w-100" controls>
                                            <source src="{{asset('storage/'.$p_row->file)}}" type="video/mp4">
                                        </video>
                                @endif
    {{--                            <a href="#" class="fw-600 text-primary ms-2 d-inline-block ml-1">See more</a>--}}
                            </p>
                        </div>
                        <div class="card-body d-flex p-0">
                            <a href="javascript:void(0)" class="d-flex align-items-center fw-600 text-grey-900 text-dark lh-26 font-xssss me-2" id="postLikeCount">
{{--                                <i class="far fa-thumbs-up text-white bg-primary-gradiant me-1 btn-round-xs font-xss"></i>--}}
                                @if(@$p_row->is_like)
                                <i class="far fa-heart text-white bg-red-gradiant me-2 btn-round-xs font-xss post_like" postIndex="{{$p}}" ></i>
                                @else
                                <i class="far fa-heart text-white bg-primary-gradiant me-2 btn-round-xs font-xss post_like" postIndex="{{$p}}"></i>
                                @endif
                                <span class="like_count_val">{{$p_row->like_counts}} Like</span>
                            </a>
                            <a href="#" class="d-flex align-items-center fw-600 text-grey-900 text-dark font-xssss">
                                <i class="far fa-comment text-dark text-grey-900 btn-round-sm"></i>
                                <span class="d-none-xss">
                                    22 Comment
                                </span>
                            </a>
                            <a href="#" class="ms-auto d-flex align-items-center fw-600 text-grey-900 text-dark font-xssss"><i class="fas fa-share-alt text-grey-900 text-dark btn-round-sm"></i><span class="d-none-xs">Share</span></a>
                        </div>
                    </div>
                    @endforeach
                @endif
{{--                <div class="card w-100 shadow-xss rounded-xxl border-0 p-4 mt-2">--}}
{{--                    <div class="card-body p-0 d-flex">--}}
{{--                        <figure class="avatar me-3 m-0"><img src="assets/imgs/2.jpg" alt="image" class="shadow-sm rounded-circle w45"></figure>--}}
{{--                        <h4 class="fw-700 text-grey-900 font-xssss mt-1 ml-2">Anthony Daugloi  <span class="d-block font-xssss fw-500 mt-1 lh-3 text-grey-500">2 hour ago</span></h4>--}}
{{--                        <a href="#" class="ms-auto"><i class="fas fa-ellipsis-h text-grey-900 btn-round-md bg-greylight font-xss"></i></a>--}}
{{--                    </div>--}}
{{--                    <div class="card-body p-0 mb-3 mt-2 rounded-3 overflow-hidden">--}}
{{--                        <video autoplay="" loop="" class="float-right w-100" controls>--}}
{{--                            <source src="assets/vedio/1.mp4" type="video/mp4">--}}
{{--                        </video>--}}
{{--                    </div>--}}
{{--                    <div class="card-body p-0 me-lg-5">--}}
{{--                        <p class="fw-500 text-grey-500 lh-26 font-xssss w-100">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi nulla dolor, ornare at commodo non, feugiat non nisi. Phasellus faucibus mollis pharetra. Proin blandit ac massa sed rhoncus <a href="#" class="fw-600 text-primary ms-2 d-inline-block ml-1">See more</a></p>--}}
{{--                    </div>--}}
{{--                    <div class="card-body d-flex p-0">--}}
{{--                        <a href="#" class="d-flex align-items-center fw-600 text-grey-900 text-dark lh-26 font-xssss me-2"><i class="far fa-thumbs-up text-white bg-primary-gradiant me-1 btn-round-xs font-xss"></i><i class="far fa-heart text-white bg-red-gradiant me-2 btn-round-xs font-xss"></i>2.8K Like</a>--}}
{{--                        <a href="#" class="d-flex align-items-center fw-600 text-grey-900 text-dark font-xssss"><i class="far fa-comment text-dark text-grey-900 btn-round-sm"></i><span class="d-none-xss">22 Comment</span></a>--}}
{{--                        <a href="#" class="ms-auto d-flex align-items-center fw-600 text-grey-900 text-dark font-xssss"><i class="fas fa-share-alt text-grey-900 text-dark btn-round-sm"></i><span class="d-none-xs">Share</span></a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="card w-100 shadow-xss rounded-xxl border-0 p-4 mb-0 mt-2">--}}
{{--                    <div class="card-body p-0 d-flex">--}}
{{--                        <figure class="avatar me-3 m-0"><img src="assets/imgs/2.jpg" alt="image" class="shadow-sm rounded-circle w45"></figure>--}}
{{--                        <h4 class="fw-700 text-grey-900 font-xssss mt-1 ml-2">Anthony Daugloi <span class="d-block font-xssss fw-500 mt-1 lh-3 text-grey-500">2 hour ago</span></h4>--}}
{{--                        <a href="#" class="ms-auto"><i class="fas fa-ellipsis-h text-grey-900 btn-round-md bg-greylight font-xss"></i></a>--}}
{{--                    </div>--}}
{{--                    <div class="card-body p-0 me-lg-5">--}}
{{--                        <p class="fw-500 text-grey-500 lh-26 font-xssss w-100">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi nulla dolor, ornare at commodo non, feugiat non nisi. Phasellus faucibus mollis pharetra. Proin blandit ac massa sed rhoncus <a href="#" class="fw-600 text-primary ms-2 d-inline-block ml-1">See more</a></p>--}}
{{--                    </div>--}}
{{--                    <div class="card-body d-block p-0 mb-3">--}}
{{--                        <div class="row ps-2 pe-2 mb-5">--}}
{{--                            <div class="col-xs-6 col-sm-6 p-1"><a href="assets/imgs/building1.jpg" data-lightbox="roadtri"><img src="assets/imgs/building1.jpg" class="img2 rounded-3 w-100" alt="image"></a></div>--}}
{{--                            <div class="col-xs-6 col-sm-6 p-1"><a href= "assets/imgs/building1.jpg"><img src="assets/imgs/building2.jpg" class="img3 rounded-3 w-100" alt="image"></a></div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="card-body d-flex p-0">--}}
{{--                        <a href="#" class="d-flex align-items-center fw-600 text-grey-900 text-dark lh-26 font-xssss me-2"><i class="far fa-thumbs-up text-white bg-primary-gradiant me-1 btn-round-xs font-xss"></i><i class="far fa-heart text-white bg-red-gradiant me-2 btn-round-xs font-xss"></i>2.8K Like</a>--}}
{{--                        <a href="#" class="d-flex align-items-center fw-600 text-grey-900 text-dark font-xssss"><i class="far fa-comment text-dark text-grey-900 btn-round-sm"></i><span class="d-none-xss">22 Comment</span></a>--}}
{{--                        <a href="#" class="ms-auto d-flex align-items-center fw-600 text-grey-900 text-dark font-xssss"><i class="fas fa-share-alt text-grey-900 text-dark btn-round-sm"></i><span class="d-none-xs">Share</span></a>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>

    </div>
    <div class="w-5">
    </div>
@endsection

@push('js')
    <script type="text/javascript">
        $(document).ready(function(){
            var maxLength = 300;
            $(".show-read-more").each(function(){
                var myStr = $(this).text();
                if($.trim(myStr).length > maxLength){
                    var newStr = myStr.substring(0, maxLength);
                    var removedStr = myStr.substring(maxLength, $.trim(myStr).length);
                    $(this).empty().html(newStr);
                    $(this).append(' <a href="javascript:void(0);" class="read-more">...read more</a>');
                    $(this).append('<span class="more-text">' + removedStr + '</span>');
                }
            });
            $(".read-more").click(function(){
                $(this).siblings(".more-text").contents().unwrap();
                $(this).remove();
            });
        });
    </script>
    <script type="text/javascript">
        $(document).on('click','.post_like',function(e){
            e.preventDefault();
            // console.log($(this).siblings().find('span').text());

            let post_index = $(this).attr('postIndex');
            console.log($(this).attr('postIndex'));
          let post_arr =  $('#userPostsArr').val();
            post_arr = JSON.parse(post_arr);
            // console.log(post_arr[post_index])
            let index_object = post_arr[post_index];
            // console.log(index_object);
            var formData = {
                uuid : index_object.uuid,
                post_type: 'user',
                _token:"{{ csrf_token() }}"
            };

            $.ajax({
                type: "POST",
                url:'/save-post-like',
                data: formData,
                success:function(data){
                    let res = JSON.parse(data);
                    if(res.success == true){
                        if (res.target == 'like'){

                        }
                        if(res.target == 'unlike'){

                        }
                        console.log('------');
                        console.log(res.like_count);
                        let new_obj = post_arr[post_index].like_counts = res.like_count
                        console.log(post_arr);
                        // window.location.reload;
                        window.location.reload();
                    }



                }
            })


        })
    </script>

@endpush
