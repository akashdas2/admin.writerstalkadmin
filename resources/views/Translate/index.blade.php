@extends('layouts.main')
@section('title')
    Translate
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">


            <div class="white-box">
                <div class="table-responsive">
                    <table class="table text-nowrap" id="translateTextTable">
                        <thead>
                        <tr>
                            <th class="border-top-0">Translate Text From</th>
                            <th class="border-top-0">Translate Text To</th>
                            <th class="border-top-0">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(sizeof($text_lists)>0)
                            @foreach($text_lists as $t=> $row)
                            <tr>
                                <td>{{$row->from_text}}</td>
                                <td>{{$row->to_text}}</td>
                                <td>{{$row->language}}</td>
                                <td>

                                    <a href="{{url('edit-text/'.$row->uuid)}}" type="button" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                    <a href="{{url('delete-text/'.$row->uuid)}}" type="button" class="btn btn-danger"><i class="far fa-trash-alt"></i></a>

                                </td>
                            </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('js')
<script type="text/javascript">
    $(document).ready( function () {
        $('#translateTextTable').DataTable();
    } );

</script>

@endpush
