@extends('layouts.main')
@section('title')
    Translate
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{route('adminSubmitText')}}" method="POST" enctype="multipart/form-data" >

            <div class="white-box">
                <h4>Add Translated Text</h4>
                <hr>
                    @csrf
                    <input type="hidden" name="text_uuid" value="{{@$text_data->uuid}}">
                    <div class="row">
                        <div class="col">
                            <input type="text" name="text_from" class="form-control" placeholder="Text Translate From" value="{{@$text_data->from_text}}">
                        </div>
                        <div class="col">
                            <select class="form-control" name="from_text_language">
                                <option selected disabled>Select From Text Language </option>
                                <option @if(@$text_data->to_language == 'en') selected @endif value="en">English</option>
                                <option @if(@$text_data->to_language == 'sv') selected @endif value="sv">Swedish</option>
                                <option @if(@$text_data->to_language == 'da') selected @endif value="da">Danish</option>
                                <option @if(@$text_data->to_language == 'no') selected @endif value="no">Norwegian</option>
                                <option @if(@$text_data->to_language == 'fi') selected @endif value="fi">Finish</option>
                                <option @if(@$text_data->to_language == 'nl') selected @endif value="nl">Dutch</option>
                                <option @if(@$text_data->to_language == 'de') selected @endif value="de">German</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col">
                            <input type="text" name="text_to" class="form-control" placeholder="Text Translate To" value="{{@$text_data->to_text}}">
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col">
                            <input type="submit" class="btn btn-primary" id="submitRecordBtn" value="Add Record">
                        </div>
                    </div>


            </div>
            </form>
        </div>
    </div>

@endsection
