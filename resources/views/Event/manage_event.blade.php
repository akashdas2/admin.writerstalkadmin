@extends('layouts.main')
@section('page_title') Manage Event Manager Page @endsection
@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form class="row" action="{{url('submit-manager-setup')}}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="language" value="en">
        <div class="col-md-6">
            <!-- <select class="form-select" name="language" aria-label="Default select example">
                <option @if(@!$manager_screen_data) selected @endif disabled>Language</option>
                <option @if(@$manager_screen_data->language == 'en') selected @endif value="en">English</option>
                <option @if(@$manager_screen_data->language == "sv") selected @endif value="sv">Swedish</option>
                <option @if(@$manager_screen_data->language == "da") selected @endif value="da">Danish</option>
                <option @if(@$manager_screen_data->language == "no") selected @endif value="no">Norwegian</option>
                <option @if(@$manager_screen_data->language == "fi") selected @endif value="fi">Finish</option>
                <option @if(@$manager_screen_data->language == "nl") selected @endif value="nl">Dutch</option>
                <option @if(@$manager_screen_data->language == "de") selected @endif value="de">German</option>
            </select> -->
        </div>
        <div class="row pt-4">
            <div class="col-md-3 py-2 text-center img_selection">
                <p>Event Managers Screen Image</p>
                <img id="blah" class="product-img" @if(@$manager_screen_data->image) src="{{asset($manager_screen_data->image)}}" @else src="{{asset('template/images/placeholder.png')}}" @endif  alt="your image" />
                <input  type='file' name="image" class="choose-file"/>
            </div>
            <div class="col-md-9">
                <label class="form-check-label" for="exampleCheck1">Text after event managers login</label>
                <textarea class="form-control " name="text_after_login" placeholder="Write here" id="" style="height: 80px">{{@$manager_screen_data->text_after_login}}</textarea>
            </div>
        </div>

        <div class="mb-2">
            <label class="form-check-label" for="exampleCheck1">Create New Event</label>
            <textarea class="form-control " placeholder="Here you plan your own mobile treasure hunt:
Read the guide that you have received by email before you get started.
You can create a treasure hunt with up to 10 teams.
" id="" name="create_new_event_text" style="height: 80px">{{@$manager_screen_data->create_new_event_text}}</textarea>
        </div>
        <div class="row mb-2">
            <div class="col-md-6">
                <label for="exampleInputEmail1" class="form-label">Activation Code</label>
                <textarea class="form-control " name="activation_code_text" placeholder="" id="" style="height: 80px">{{@$manager_screen_data->activation_code_text}}</textarea>
                <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
            </div>
            <div class="col-md-6">
                <label for="exampleInputPassword1" class="form-label">Phone Number</label>
                <textarea class="form-control " name="phone_number_text" placeholder="Input your mobile phone number. You will receive a link to the organizer page so that you can keep track of the teams and start the different teams using your mobile." id="" style="height: 80px">{{@$manager_screen_data->phone_number_text}}</textarea>
            </div>
        </div>

        <div class="mb-2">
            <label class="form-check-label" for="exampleCheck1">Text that explains how to put the pins on the map.</label>
            <textarea class="form-control " placeholder="Product and language specific and also a image for each product.
can be the same image as the above “Select image to be shown in the event managers screen after login:”
The treasure hunt questions and clues are prepared and with multiple choice answers. Now you can place where the questions should on the map and also the finishing point of the treasure hunt. Zoom into the area you would like the treasure hunt to take place. Click on the button 'Location' for Q1, then click where on the map you want to place it. If you want to move a Question just click again and the new location. Continue until all the questions are on the map and also the final destination where the treasure or meeting point should be.
            " id="" name="how_to_pin_map_text" style="height: 80px">{{@$manager_screen_data->how_to_pin_map_text}}</textarea>
        </div>
        <div class="mb-2">
            <label class="form-check-label" for="exampleCheck1">Text that explains how you can start the teams and overview the progress of the teams.</label>
            <textarea class="form-control " name="overview_text" placeholder="Here you manage the treasure hunt and can start the different teams at different time. The time for each team will start when the team logs in and reads the story of the treasure hunt. You can keep track of the progress of the teams in terms of point and their time. Each team´s time will stop when they have reach and clicked on the final pinpoint. When all the teams are done the points for the fastest team will be distributed by the system. If a team does not finish you can exclude the team from the calculation using the exclude button in order to distribute the time points to the teams. The team with the highest score will win." id="" style="height: 80px">{{@$manager_screen_data->overview_text}}</textarea>
        </div>

        <div class="mb-2">
            <label class="form-check-label" for="exampleCheck1">Final Result </label>
            <textarea class="form-control " name="final_result_text" placeholder="Fill out your mobile number. You get an sms with a shortlink to your setup-site to easily reach it for starting teams and viewing results. Fill out your mobile number. You get an sms with a shortlink to your setup-site to easily reach it for starting teams and viewing results" id="" style="height: 80px">{{@$manager_screen_data->final_result_text}}</textarea>
        </div>

        <div class="text-center pt-4">
            <button class="btn-success text-white btn submit_form col-md-12" type="submit">Submit</button>
        </div>
    </form>

@endsection
@section('scripts')
    <script src="{{asset('template/js/event.js')}}"></script>

@endsection
