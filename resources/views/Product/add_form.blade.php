@extends('layouts.main')
@section('page_title') Product @endsection


@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="alert alert-danger alert-dismissible alertMessage " style="display: none">
        <strong>Sorry! </strong> Failed to save record.
    </div>


    <form id="product_form" action="{{url('submit-product')}}" method="POST" enctype="multipart/form-data" class="row">
        @csrf
        <input id="questions_data" type="hidden" name="questions_data" value="">
        <input type="hidden" name="p_id"  value="{{@$product_data->id}}">
        <div class="col-md-4">
            <select class="form-select" name="language" aria-label="Default select example">
                <option disabled>Language</option>
                <option @if(@$product_data->language == "en") selected @endif value="en">English</option>
                <option @if(@$product_data->language == "sv") selected @endif value="sv">Swedish</option>
                <option @if(@$product_data->language == "da") selected @endif value="da">Danish</option>
                <option @if(@$product_data->language == "no") selected @endif value="no">Norwegian</option>
                <option @if(@$product_data->language == "fi") selected @endif value="fi">Finish</option>
                <option @if(@$product_data->language == "nl") selected @endif value="nl">Dutch</option>
                <option @if(@$product_data->language == "de") selected @endif value="de">German</option>
            </select>
        </div>
        <div class="col-md-4">
            <input type="text" name="product_id" class="form-control" id="productId" placeholder="Product ID" value="{{@$product_data->product_id}}">
            <div>
            <p class="text-danger mt-2" id="productMsg" style="font-weight: 700; margin-left: 8px;"></p>
            </div>
        </div>
        <div class="col-md-4">
            <select class="form-select" name="punishment">
                <option disabled>Punishment Time (s)</option>
                <option @if(@$product_data->punishment_time == "0") selected @endif value="0">No Punishment Time </option>
                <option @if(@$product_data->punishment_time == "20") selected @endif value="20">20</option>
                <option @if(@$product_data->punishment_time == "30") selected @endif value="30">30</option>
                <option @if(@$product_data->punishment_time == "40") selected @endif value="40">40</option>
            </select>
        </div>

        <div class="col-md-4">
            <select class="form-select" name="compete_teams">
                <option disabled selected>Teams can compete</option>
                <option @if(@$product_data->compete_teams == "5") selected @endif value="5">5</option>
                <option @if(@$product_data->compete_teams == "10") selected @endif value="10">10</option>
                <option @if(@$product_data->compete_teams == "20") selected @endif value="20">20</option>
                <option @if(@$product_data->compete_teams == "30") selected @endif value="30">30</option>
                <option @if(@$product_data->compete_teams == "50") selected @endif value="50">50</option>
                <option @if(@$product_data->compete_teams == "70") selected @endif value="70">70</option>
                <option @if(@$product_data->compete_teams == "100") selected @endif value="100">100</option>
            </select>

        </div>

        <div class="row">
            <div class="col-md-8 mt-5">
                <input type="checkbox" name="extra_intro_message" value="1" @if(@$product_data->extra_intro_message == 1) checked @endif >
                <label> <strong> Let Event Manager write an extra intro message that is show after scanning the QR code.</strong></label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 mt-5">
                <input type="checkbox" name="last_screen_message" value="1" @if(@$product_data->last_screen_message == 1) checked @endif >
                <label> <strong> Let Event Manager write a message for the  last screen of the app.</strong></label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 mt-5">
                <input type="checkbox" name="display_hidden_value" value="1" @if(@$product_data->display_hidden_value == 1) checked @endif >
                <label> <strong> Bring forward hidden value from question in order to used and displayed on the last screen.</strong></label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 mt-5">
                <input type="checkbox" name="show_total_points" value="1" @if(@$product_data->show_total_points == 1) checked @endif >
                <label> <strong>Let user show the total points on the last screen of the app.</strong></label>
            </div>
        </div>

        <div class="col-md-12 p-4">
            <h4>The text and image that is shown once you have scanned the QR code</h4>
            <div class="row border-dark">
                <div class="col-md-3 py-2 text-center img_selection">
                    <p>Upload image to be shown on the story board</p>
                    <img id="blah" class="product-img" @if(@$product_data->story_board_image) src="{{asset($product_data->story_board_image)}}" @else src="{{asset('template/images/placeholder.png')}}" @endif alt="your image" />
                    <input type='file' name="story_board_image" id="boardImage" class="choose-file"/>
                    <div>
                        <p class="text-danger mt-2" id="boardImgMsg" style="font-weight: 700; margin-left: 8px;"></p>
                    </div>
                </div>
                <div class="col-md-9">

                    <textarea name="story_board_text" class="ckeditor" id="description-product" placeholder="Story Board text" >{{@$product_data->story_board_text}}
                    </textarea>
                </div>
            </div>
        </div>

        <div class="col-md-12 p-4">
            <h4>The text and image that is shown once you have end game</h4>
            <div class="row border-dark">
                <div class="col-md-3 py-2 text-center img_selection">
                    <p>Upload image to be shown at end of the game</p>
                    <img id="blah" class="product-img" @if(@$product_data->goal_image) src="{{asset($product_data->goal_image)}}" @else src="{{asset('template/images/placeholder.png')}}" @endif alt="your image" />
                    <input type='file' name="goal_image" id="boardImage" class="choose-file" />
                    <div>
                        <p class="text-danger mt-2" id="boardImgMsg" style="font-weight: 700; margin-left: 8px;"  ></p>
                    </div>
                </div>
                <div class="col-md-9">

                    <textarea name="goal_text" class="ckeditor" id="description-product" placeholder="Story Board text" >{{@$product_data->goal_text}}

                    </textarea>
                </div>
            </div>
        </div>

        <div class="col-md-12 p-4">
            {{--            <h4>The text and image that is shown once you have scanned the QR code</h4>--}}
            <h4>Upload guide pdf for product</h4>
            <div class="col-md-3 py-2 text-center img_selection">
                {{--                <p></p>--}}
                <iframe  name="product_guide" class="product-img" id=""  @if(@$product_data->guide_pdf) src="{{asset($product_data->guide_pdf)}}" @else src="{{asset('template/images/placeholder_pdf.png')}}" @endif alt="guide pdf" style="width:400px; height:195px;" frameborder="0"></iframe>
                <input type='file' name="guide_pdf" class="choose-file" id="guidePdf" accept="application/pdf"/>
                <div>
                    <p class="text-danger mt-2" id="guidePdfMsg" style="font-weight: 700; margin-left: 8px;"></p>
                </div>
            </div>
        </div>
        <h2 class="text-center"><strong>Email Content</strong></h2>
           <div class="col-md-4">
                <input type="text" name="email_subject" class="form-control" id="emailSubject" placeholder="Subject" value="{{@$product_data->email_subject}}">
               <div>
                   <p class="text-danger mt-2" id="emailSubjectMsg" style="font-weight: 700; margin-left: 8px;"></p>
               </div>
           </div>
        <div class="text-center">
            <h4 class="" style="font-weight: 800">Email Body</h4>
        </div>
        <div class="col-md-12">
 <textarea class="form-control ckeditor" name="email_body" id="emailBody" rows="4">
                                        {{--    You have ordered the Spy Hunt - a fun GPS Treasure Hunt made on for smartphones. Start by reading the guide you received from us by e-mail. The guide comes in a separate mail so you if you cannot find in your inbox, also check the spam folder. In the guide you will get all the information you need to easily prepare and carry out the game.

Through the link below you will find your organizer site where you create your unique treasure hunt wherever you want. Your organizer site can be accessed through the following unique
link: [link]
This link and activation code you need to create your GPS Treasure Hunt. You also need the link to later enter your organizer site so save this email.

When you create the Treasure Hunt, enter your own mobile number and send the link to your mobile phone if you want to be mobile when you perform the mobile hunt. Below you will see an activation code. NOTE! The activation code works once, to create and implement a treasure hunt for up to 10 teams. When the hunt is completed, the activation code has expired. Your activation code: [code] Make a test run first! You also get a test code.

With this you can create your own test event and test the full hunt on your own mobile to find out how everything works before you implement it. To the real event, use the activation code above.

To carry out the hunt, you need to download our app on any phone to use. Apps for iOS and Android can be found by searching "Grapevine Treasure Hunt" in the app Store or Google Play or through links on this page:
https://www.grapevine.nu/grapevinetreasurehunt
 NOTE! In order for the spy hunt to work optimally, the phone must be in contact with three satellites. It may take up to 3 minutes. Therefore, turn on the location service on the phones 5 minutes before running the app and the hunt on your phone if you do not already have location services running. Preparing and implementing the treasure hunt is not difficult or difficult, but please read the guide first!
If you have any questions, please contact us at info@grapevine.nu. --}}
{{@$product_data->email_body}}
</textarea>
    </div>
        <div class="col-md-12">
            <p class="text-danger mt-2" id="emailBodyMsg" style="font-weight: 700; margin-left: 8px;"></p>
        </div>
        <h2 class="text-center my-3"><strong> Questions Area</strong></h2>
        <div>
            <p class="text-danger mt-2" id="questionMsg" style="font-weight: 700; margin-left: 8px; margin-bottom:0;"></p>
        </div>
        <div class="col-md-12 p-3">

            <div class="add_question_area">
                @if(@$product_data)
                    @if(sizeof($product_data->questions) > 0)
                    @foreach($product_data->questions as $key => $question)
                            <div class="border-dark p-3 pb-0 my-3 question_area">
                                <h4>Question {{$key+1}}</h4>
                                <input type="hidden" class="q_id" value={{$question->id}}>
                                <input class="question_number" type="hidden" value="{{sizeof($product_data->questions)+1}}">
                                <textarea class="form-control form_question " id="exampleFormControlTextarea1" rows="4">{{$question->question}}</textarea>
                                <h4 class="mt-4">Answer</h4>
                                <div class="row">

                                    <div class="add_answers_area row">
                                        @if(sizeof($question->answers) > 0)
                                            <input class="answer_number" type="hidden" value="{{sizeof($question->answers)+1}}">
                                            @foreach($question->answers as $answer_key => $answer)
                                                <div class="col-md-6 p-2"><input type="text" class="form-control form_answer" a_id={{$answer->id}} id="" placeholder="Answer {{$answer_key+1}}" value="{{$answer->Answer}}"></div>

                                            @endforeach
                                            @endif
                                        {{--<div class="col-md-6 p-2"><input type="text" class="form-control form_answer" a_id=0 id="" placeholder="Answer 2"></div>--}}
                                        {{--<div class="col-md-6 p-2"><input type="text" class="form-control form_answer" a_id=0 id="" placeholder="Answer 3"></div>--}}
                                        {{--<div class="col-md-6 p-2"><input type="text" class="form-control form_answer" a_id=0 id="" placeholder="Answer 4"></div>--}}

                                    </div>
                                    <div class="col-md-12">
                                        <button class="btn-danger text-white btn add_more_answers" type="button">Add More</button>
                                    </div>
                                    <h4 class="mt-4 col-md-12">Correct Answer <span class="text-muted" style="font-size: 13px" > e.g(A, B ,C, e.t.c)</span></h4>
                                    <div class="col-md-6 p-2">
                                        <input type="text" class="form-control form_correct_answer" id="" placeholder="correct Answer" value="{{$question->correct_answer}}">
                                    </div>
                                    <!-- IMAGE AND TEXTAREA -->
                                    <div class="col-md-12 p-4">
                                        <div class="row border-dark">
                                            <div class="col-md-3 py-2 text-center img_selection">
                                                <img id="blah" class="product-img" @if($question->question_image) src="{{asset($question->question_image)}}" @else  src="{{asset('template/images/placeholder.png')}}" @endif alt="your image" />
                                                <input type='file' class="choose-file form_question_image"/>
                                            </div>
                                            <div class="col-md-9">
                                                <textarea id="description-product" class="form_question_details" >{{@$question->question_text}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    @endif

                @else
                    <div class="border-dark p-3 pb-0 my-3 question_area">
                        <h4>Question 1</h4>
                        <input type="hidden" class="q_id" value=0>
                        <input class="question_number" type="hidden" value="2">
                        <textarea class="form-control form_question " id="exampleFormControlTextarea1" rows="4"></textarea>
                        <h4 class="mt-4">Answer</h4>
                        <div class="row">
                            <input class="answer_number" type="hidden" value="5">
                            <div class="add_answers_area row">
                                <div class="col-md-6 p-2"><input type="text" class="form-control form_answer" a_id=0 id="" placeholder="Answer 1"></div>
                                <div class="col-md-6 p-2"><input type="text" class="form-control form_answer" a_id=0 id="" placeholder="Answer 2"></div>
                                <div class="col-md-6 p-2"><input type="text" class="form-control form_answer" a_id=0 id="" placeholder="Answer 3"></div>
                                <div class="col-md-6 p-2"><input type="text" class="form-control form_answer" a_id=0 id="" placeholder="Answer 4"></div>

                            </div>
                            <div class="col-md-12">
                                <button class="btn-danger text-white btn add_more_answers" type="button">Add More</button>
                            </div>
                            <h4 class="mt-4 col-md-12">Correct Answer <span class="text-muted" style="font-size: 13px" > e.g(A, B ,C, e.t.c)</span></h4>
                            <div class="col-md-6 p-2">
                                <input type="text" class="form-control form_correct_answer" id="" placeholder="Correct Answer"  value="" onkeypress="return lettersOnly(event)"  maxlength="1">
                            </div>
                            <!-- IMAGE AND TEXTAREA -->
                            <div class="col-md-12 p-4">
                                <div class="row border-dark">
                                    <div class="col-md-3 py-2 text-center img_selection">
                                        <img id="blah" class="product-img" src="{{asset('template/images/placeholder.png')}}" alt="your image" />
                                        <input type='file' class="choose-file form_question_image"/>
                                    </div>
                                    <div class="col-md-9">
                                        <textarea id="description-product" class="form_question_details " ></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                @endif

            </div>
            <div class="text-center pt-4">
                <button class="btn-danger text-white btn add_question" type="button">Add More Questions</button>
            </div>
        </div>
        <div class="text-center pt-4">
            <button class="btn-success text-white btn submit_form col-md-12" type="button">Submit</button>
        </div>
    </form>
@endsection
@section('scripts')
    <script src="{{asset('template/js/product.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.ckeditor').ckeditor();
        });
          CKEDITOR.replace('ckeditor1');
    </script>

@endsection
