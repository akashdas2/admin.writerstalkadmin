
@extends('layouts.main')
@section('page_title') List Products @endsection


@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table class="table text-nowrap">
                        <thead>
                        <tr>
                            <th class="border-top-0">Product ID</th>
                            <th class="border-top-0">Punishment Time</th>
                            <th class="border-top-0">Language</th>
                            <th class="border-top-0">Questions</th>
                            <th class="border-top-0">Action</th>
                            <!-- <th class="border-top-0">Create New Product</th> -->
                        </tr>
                        </thead>
                        <tbody>
                        @if(sizeof($products) > 0)

                            @foreach($products as $product)
                        <tr>
                            <td>{{$product->product_id}}</td>
                            <td>{{$product->punishment_time}}</td>
                            <td>{{$product->language}}</td>
                            <td>{{$product->questions}}</td>
                            <td>
                                <a href="{{url('edit-product/'.$product->uuid)}}" type="button" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                <a href="{{url('delete-product/'.$product->uuid)}}" type="button" class="btn btn-danger"><i class="far fa-trash-alt"></i></a>
                                <a href="{{url('copy-product/'.$product->uuid)}}" class="btn btn-success" id="create_new" type="button" >Create New</a>
                            </td>
                        </tr>
@endforeach

                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('template/js/product.js')}}"></script>
@endsection

