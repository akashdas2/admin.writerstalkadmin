@extends('layouts.main')
@section('page_title') Product @endsection


@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="alert alert-danger alert-dismissible alertMessage " style="display: none">
        <strong>Sorry! </strong> Failed to save record.
    </div>


    <form id="product_form" action="{{url('submit-pro-product')}}" method="POST" class="row">
        @csrf
        <input id="questions_data" type="hidden" name="questions_data" value="">
        
        <div class="col-md-4">
            <select class="form-select" name="language" aria-label="Default select example">
                <option disabled>Language</option>
                <option value="en">English</option>
                <option value="sv">Swedish</option>
                <option value="da">Danish</option>
                <option value="no">Norwegian</option>
                <option value="fi">Finish</option>
                <option value="nl">Dutch</option>
                <option value="de">German</option>
            </select>
        </div>

        <div class="col-md-4">
            <input type="text" name="product_id" class="form-control" id="productId" placeholder="Product ID" value="">
            <div>
            <p class="text-danger mt-2" id="productMsg" style="font-weight: 700; margin-left: 8px;"></p>
            </div>
        </div>
        <div class="col-md-4">
            <select class="form-select" name="punishment">
                <option disabled>Punishment Time (s)</option>
                <option>No Punishment Time </option>
                <option>20</option>
                <option>30</option>
                <option>40</option>
            </select>
        </div>

         <div class="col-md-4">
            <select class="form-select" name="compete_teams">
                <option disabled selected>Teams can compete</option>
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="30">30</option>
                <option value="50">50</option>
                <option value="70">70</option>
                <option value="100">100</option>
            </select>

        </div>

         <h2 class="text-center"><strong>Email Content</strong></h2>
           <div class="col-md-4">
                <input type="text" name="email_subject" class="form-control" id="emailSubject" placeholder="Subject" value="" >
               <div>
                   <p class="text-danger mt-2" id="emailSubjectMsg" style="font-weight: 700; margin-left: 8px;"></p>
               </div>
           </div>

           <div class="text-center">
            <h4 class="" style="font-weight: 800">Email Body</h4>
        </div> 
        <div class="col-md-12">
        <textarea class="form-control ckeditor" name="email_body" id="emailBody" rows="4">
        </textarea>
        </div>

        <button class="btn-success text-white btn submit_form col-md-12" type="submit">Submit</button>
    </form>
@endsection