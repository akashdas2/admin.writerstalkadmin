@extends('admin.layouts.main')

@section('title')
    Create New Post
@endsection
@push('style')
    <style>
    .container {
        max-width: 500px;
    }
    dl, ol, ul {
        margin: 0;
        padding: 0;
        list-style: none;
    }
    #thumbnail-action {
        align-items: center;
        margin-bottom: 10px;
    }
    </style>

@endpush

@section('content')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" />
    <script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>

    <div class="">
        <form action="{{route('admin.posts.update', $admin_post->uuid)}}" method="post" enctype="multipart/form-data">
            @method('PUT')
            {{ csrf_field() }}
            <div class="card">
                <div class="card-body">
                    <input type="hidden" name="file_extension" id="FileExtension" value="">
                    <input type="hidden" name="thumbnail_extension" id="FileExtension-2" value="">
                    <h3 class="text-center mb-5">Upload File </h3>
                    @csrf
        {{--            @if ($message = Session::get('success'))--}}
        {{--                <div class="alert alert-success">--}}
        {{--                    <strong>{{ $message }}</strong>--}}
        {{--                </div>--}}
        {{--            @endif--}}
        
        {{--            @if (count($errors) > 0)--}}
        {{--                <div class="alert alert-danger">--}}
        {{--                    <ul>--}}
        {{--                        @foreach ($errors->all() as $error)--}}
        {{--                            <li>{{ $error }}</li>--}}
        {{--                        @endforeach--}}
        {{--                    </ul>--}}
        {{--                </div>--}}
        {{--            @endif--}}
                    <div class="alert alert-success alert-block" id="responseMsg" style="display: none">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong></strong>
                    </div>
        
                    <div class="form-group row">
                        <div class="col-md-6 col-sm-12">
                            <h2>File</h2>
                            @if($admin_post->file_type == 'image')
                                <img src="{{ asset('storage/'.$admin_post->file) }}" class="myImage" width="320" height="240" style="display: {{ $admin_post->file == '' ? 'none' : 'block' }}">
                            @else
                                <video  id="video_p" class="myVideo"  width="320" height="240" controls style="display: {{ $admin_post->file == '' ? 'none' : 'block' }}">
                                    <source src="{{ asset('storage/'.$admin_post->file) }}" type="video/mp4">
                                </video>
                            @endif
                            <div id="thumbnail-container" style="display: none">
                                <canvas id="canvas-element"></canvas>
                                 <div class="d-flex" id="thumbnail-action">
                                     Seek to <select id="set-video-seconds" class="mx-1"></select> <a id="get-thumbnail" href="#">Download Thumbnail</a>
                                 </div>
                            </div>
                            
                            <div class="custom-file">
                                <input type="file" name="file" class="custom-file-input" id="chooseFile" onchange="readFile(this)">
                                <label class="custom-file-label file-label" for="chooseFile">Select file</label>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12" id="thumbnail-block">
                            <h2>Thumbnail</h2>
                            <img src="{{ asset('storage/'.$admin_post->thumbnail) }}" class="thumbnail" style="display: {{ $admin_post->thumbnail != '' ? 'block' : 'none' }};width: 320px;height:240px;">
                             <div class="custom-file">
                                <input type="file" name="thumbnail" class="custom-file-input-2" id="thumbnail" onchange="readThumbnail(this)">
                                <label class="custom-file-label file-label-2" for="thumbnail">Select file</label>
                            </div>
                        </div>
                    </div>
                    
                     <div class="form-group">
                        <input type="text" name="title" class="form-control" placeholder="Post title" value="{{ $admin_post->title }}" />
                    </div>
                    
                    <div class="row form-group">
                        <div class="col-md-6 col-sm-12">
                            <input type="text" name="published_at" class="form-control datetimepicker" placeholder="Publish At" value="{{ $admin_post->published_at }}"/>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <select name="tip_type" class="form-control" value="{{ $admin_post->tip_type }}" style="border: 1px solid grey;">
                                <option selected disabled>Select File Type</option>
                                <option value="pro" {{ $admin_post->tip_type == 'pro' ? 'selected' : '' }}>Pro Tips</option>
                                <option value="basic" {{ $admin_post->tip_type == 'basic' ? 'selected' : '' }}>Basic</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <textarea class="ckeditor" id="editor0" name="description">{{ $admin_post->description }}</textarea>
                    </div>
                    
                    <div class="from-group">
                         <select class="category related-post form-control select mt-3" value="{{ $admin_post->post_tags }}" name="post_tags[]" multiple>
                            @if(sizeof(@$tags))
                                @foreach(@$tags as $t=>$t_row)
                                    <option value="{{@$t_row->uuid}}">{{@$t_row->tag_name}}</option>
                                @endforeach
                            @endif
                        </select>
                        <script>
                            $(".select").val(@json($admin_post->post_tags)).select2().trigger("change");
                        </script>
                    </div>
        
                    <button type="submit" name="submit" class="btn btn-primary btn-block mt-4">
                        Update Post
                    </button>
                </div>
            </div>

        </form>
    </div>

@endsection
@push('js')

<script type="text/javascript" src="{{ asset('js/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript">

    $('.datetimepicker').datetimepicker({ 
        date: new Date('{{ $admin_post->published_at }}'),
        format: 'DD-MM-YYYY hh:mm A',
        icons: {
            time: 'fa fa-clock-o',
            date: 'fa fa-calendar',
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-check',
            clear: 'fa fa-trash',
            close: 'fa fa-times'
        }
    });

    var video = document.querySelector("#video_p");
    var canvas = document.querySelector("#canvas-element");
    var ctx = canvas.getContext("2d");

    // Video metadata is loaded
    video.addEventListener('loadedmetadata', function() {
        var video_duration = video.duration,
	    	duration_options_html = '';
	    // Set options in dropdown at 4 second interval
	    for(var i=0; i<Math.floor(video_duration); i=i+4) {
	    	duration_options_html += '<option value="' + i + '">' + i + ' sec</option>';
	    }
	    document.querySelector("#set-video-seconds").innerHTML = duration_options_html;
	    // Set canvas dimensions same as video dimensions
        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;
        canvas.style.display = 'none';
        document.querySelector("#thumbnail-container").style.display = 'block';
    });
    
    // On changing the duration dropdown, seek the video to that duration
    document.querySelector("#set-video-seconds").addEventListener('change', function() {
        video.currentTime = document.querySelector("#set-video-seconds").value;
        
        // Seeking might take a few milliseconds, so disable the dropdown and hide download link 
        document.querySelector("#set-video-seconds").disabled = true;
        document.querySelector("#get-thumbnail").style.display = 'none';
    });
    
    // Seeking video to the specified duration is complete 
    document.querySelector("#video_p").addEventListener('timeupdate', function() {
    	// Re-enable the dropdown and show the Download link
    	document.querySelector("#set-video-seconds").disabled = false;
        document.querySelector("#get-thumbnail").style.display = 'inline';
    });
    
    // On clicking the Download button set the video in the canvas and download the base-64 encoded image data
    document.querySelector("#get-thumbnail").addEventListener('click', function() {
        console.log({
            video,
            width: video.videoWidth,
            height: video.videoHeight
        })
        ctx.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
        console.log({ctx, canvas});
    
    	document.querySelector("#get-thumbnail").setAttribute('href', canvas.toDataURL());
    	document.querySelector("#get-thumbnail").setAttribute('download', 'thumbnail.png');
    });

    function readFile(input) {
         let reader;
        let isImageExt = true;
        let isVideoExt = true;
        let  validImageExtensions = ["jpg","pdf","jpeg","gif","png"];
        let  validVideoExtensions = ["mp4"];
        $('#FileExtension').val('');
        if (input.files && input.files[0]) {
           let file_ext = input.files[0].name.split('.').pop().toLowerCase();
           if (validImageExtensions.indexOf(file_ext) == -1){
               isImageExt = false;
           }
           if (validVideoExtensions.indexOf(file_ext) == -1){
               isVideoExt = false;
           }

            if (isImageExt == true){
                $("#thumbnail-block").css('display', 'none');
                reader = new FileReader();
                reader.onload = function(e) {
                    $('#FileExtension').val('image');
                    $('.myVideo').hide();
                    $('.myImage').show();
                    $('.myImage').attr('src', e.target.result);
                    $('.myImage').css('opacity', 1);
                    $('.file-label').text(input.files[0].name)
                };
                reader.readAsDataURL(input.files[0]);

            }

           if (isVideoExt == true){
                $("#thumbnail-block").css('display', 'block');
               reader = new FileReader();
               reader.onload = function(e) {
                   $('#FileExtension').val('video');
                   $('.myImage').hide();
                   $('.myVideo').show();
                   $('.myVideo').attr('src', e.target.result);
                   $('.myVideo')[0].load();
                   $('.file-label').text(input.files[0].name)
               };
               reader.readAsDataURL(input.files[0]);

           }


        }
    }
    
    function readThumbnail(input) {
         let reader;
        let isImageExt = true;
        let  validImageExtensions = ["jpg","pdf","jpeg","gif","png"];
        $('#FileExtension-2').val('');
        if (input.files && input.files[0]) {
           let file_ext = input.files[0].name.split('.').pop().toLowerCase();
           if (validImageExtensions.indexOf(file_ext) == -1){
               isImageExt = false;
           }

            if (isImageExt == true){
                reader = new FileReader();
                reader.onload = function(e) {
                    $('#FileExtension-2').val('image');
                    $('.thumbnail').show();
                    $('.thumbnail').attr('src', e.target.result);
                    $('.thumbnail').css('opacity', 1);
                    $('.file-label-2').text(input.files[0].name)
                };
                reader.readAsDataURL(input.files[0]);

            }
        }
    }

    // $(document).on('click','#deleteVideoBtn',function (e){
    //
    //
    //     if(!confirm("Do you really want to do this?")) {
    //         return false;
    //     }
    //     e.preventDefault()
    //     //
    //     // let video_id = $(this).attr('videoable_id');
    //     // let video_type = $(this).attr('videoable_type');
    //
    //     var id = $(this).data("id");
    //     // var id = $(this).attr('data-id');
    //
    //     var key = $(this).attr('key_index')
    //     var token = $("meta[name='csrf-token']").attr("content");
    //     var url = e.target;
    //     console.log(id)
    //     $.ajax(
    //         {
    //             url: url.href, //or you can use url: "company/"+id,
    //             type: 'DELETE',
    //             data: {
    //                 id: id,
    //                 _token: token
    //             },
    //             success: function (response){
    //                 if (response.success == true){
    //                     $("#responseMsg").html(response.message)
    //                         location.reload()
    //                 }
    //
    //             }
    //         });
    //     return false;
    //
    //
    //
    //
    // });

</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.category').select2(
            {
                placeholder:"Select Tags",
                allowClear: true
            }
        );


    });
</script>

@endpush
