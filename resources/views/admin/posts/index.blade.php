@extends('admin.layouts.main')
@section('title')
    {{__('Users')}}
@endsection

@section('content')

    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between align-items-center mb-2">
                    <h4 class="card-title mb-0">Posts Table</h4>
                    <a href="{{ route('admin.posts.create') }}" class="user-status-btn tooltip2 btn-sm btn btn-success">
                        <i class="mdi mdi-plus" style="color: #2e47a2"></i> Post
                        <span class="tooltiptext2">Post</span>
                    </a>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped selfDataTable">
                        <thead>
                        <tr>
                            <th>File</th>
                            <th>Title</th>
                            <th>Tip</th>
                            <th>Description</th>
                            <th>Created At</th>
                            <th>Published At</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(sizeof($admin_posts)> 0)
                            @foreach($admin_posts as $u => $row)
                        <tr>
                            <td class="py-1">
                                @if($row->file_type == 'video')
                                    <video width="320" height="240" controls="">
                                        <source src="{{asset('storage/'.@$row->file)}}" type="video/mp4">
                                        <source src="movie.ogg" type="video/ogg">
                                        Your browser does not support the video tag.
                                    </video>
                                @else
                                    <img src="{{asset('storage/'.@$row->file)}}" alt="{{$row->title}}"/>
                                 @endif
                            </td>

                            <td>{{@$row->title}}</td>
                            <td>{{@$row->tip_type}}</td>
                            <td>{!! @$row->description !!}</td>
                            <td>{{\Carbon\Carbon::parse(@$row->created_at)->format('d-m-Y')}}</td>
                            <td>
                                @php
                                $cssClass = strtotime($row->published_at) > time() ? 'alert-warning' : 'alert-success';
                                @endphp
                                <span class="alert {{ $cssClass }}">{{ $cssClass == 'alert-success' ? 'Published at' : 'Will be publish after' }} {{ \Carbon\Carbon::parse($row->published_at)->diffForHumans() }}</span>
                            </td>
                            <td>
                                <a href="{{route('admin.posts.edit', $row->uuid)}}" class="user-status-btn tooltip2" style="{color: white; padding-bottom: 6px; margin-top: -10px;}">
                                    <i class="mdi mdi-panorama" style="color: #2e47a2"></i>
                                    <span class="tooltiptext2">Edit</span>
                                </a>

                                <a href="#" onclick="document.getElementById('delete-post-{{ $row->id }}').submit()" class="user-status-btn tooltip2" style="{color: white; padding-bottom: 6px; margin-top: -10px;}">
                                    <i class="mdi mdi-account-multiple" style="color: #5fdc00" ></i>
                                    <span class="tooltiptext2">Delete</span>
                                    <form id="delete-post-{{ $row->id }}" action="{{route('admin.posts.destroy', $row->uuid)}}" method="post">
                                        @method("DELETE")
                                        @csrf
                                    </form>
                                </a>

                            </td>
                        </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
