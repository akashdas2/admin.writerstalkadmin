@extends('admin.layouts.main')
@section('title')
    Quick Text
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <button type="button" class="btn btn-primary btn-sm float-right m-3 promo_code_btn" data-toggle="modal" data-target="#promoModal">
                        Add Prompt
                    </button>
                    <p class="card-title">Prompts </p>
                    <div class="table-responsive">
                        <table class="table selfDataTable">
                            <thead>
                            <tr>
                                <th>No. </th>
                                <th>File</th>
                                <th>Creator</th>
                                <th>Updated at</th>
                                <th>Created at</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(sizeof($prompts)>0)
                                @foreach($prompts as $u => $row)
                                    <tr>
                                        <td>{{ @$u + 1 }}</td>
                                        <td>
                                            <video width="400px" controls>
                                            <source src="{{ config()->get('app.front_url') }}/storage/{{ $row->file }}" type="video/mp4" alt="First slide">
                                            </video>
                                        </td>
                                        <td>
                                            {{ $row->user_details->name }}
                                        </td>
                                        <td>{{\Carbon\Carbon::parse(@$row->updated_at)->format('d-M-Y')}}</td>
                                        <td>{{\Carbon\Carbon::parse(@$row->created_at)->format('d-M-Y')}}</td>
                                        <td>
                                            <a href="#" class="btn btn-info" onclick="showData('{{ json_encode($row) }}')">Edit</a>
                                            <a href="#" class="btn btn-danger" onclick="document.getElementById('delete-{{$row->id}}').submit()">Delete</a>
                                            <form action="{{ route('admin.prompts.destroy', $row->uuid) }}" id="delete-{{ $row->id }}" method="post">
                                                @method('DELETE')
                                                @csrf
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--                END OF RECENT PURCHASES--}}
    <div class="modal fade" id="promoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Quick Text</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('admin.createPrompts')}}" method="POST" id="createPromoForm" enctype="multipart/form-data">
                        @csrf
                        <center>
                            <p class="text-danger font-weight-bold" id="responseMsg" style="display: none"></p>
                        </center>
                        <div class="form-group">
                            <label for="promo_code">File</label>
                            <input type="hidden" name="id" id="id">
                            <input type="file" name="files" id="files" multiple accept=".mov,.mp4,.x-m4v">
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary submit_promo_btn">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(document).on('click','#submitEditModalBtn', function (e){
            e.preventDefault();
            $('#updatePromoForm').submit();
        });
        $(document).on('click','#editPromoBtn', function (e){
            e.preventDefault();
            let promo_id  = $(this).attr('promo_id');
            $('#modalPromoCodeId').val(promo_id);
            let url = window.location.origin+'/admin/edit-promo/'+promo_id

            $.ajax({
                'type':'GET',
                'url': url,
                'success':function (data){
                    let get_data = JSON.parse(data);
                    if (get_data.success == true){
                        console.log(get_data.data.promo_code);
                        $('#promoCodeToEdit').val(get_data.data.promo_code);
                        // $('select#editPaymentOption option[value = get_data.data.payment_option]');
                        $('#editPaymentOption').val(get_data.data.payment_option)
                    }
                }
            })
        });
    </script>
    <script type="text/javascript">
        $(document).on('click','.submit_promo_btn',function (e){
            e.preventDefault();
            let quickText = $('#quickText').val();
            if (quickText){
                $('#responseMsg').hide();
                $('#responseMsg').html('');
                $('#createPromoForm').submit();
            }else {
                $('#responseMsg').show();
                $('#responseMsg').html('Required Fields Can Not be Empty');
            }


        })

        function showData(data)
        {
            const quick = JSON.parse(data);
            $("#quickText").val(quick.text);
            $("#id").val(quick.uuid);
            $("#promoModal").modal('show');
        }
    </script>

@endpush
