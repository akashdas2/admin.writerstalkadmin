
@extends('admin.layouts.main')
@section('title')
    {{__('Report notifications')}}
@endsection

@section('content')

    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Reports Table</h4>
                <div class="table-responsive">
                    <table class="table table-striped selfDataTable">
                        <thead>
                        <tr>
                            <th>Content Type</th>
                            <th>Content</th>
                            <th>Owner</th>
                            <th>Reporter</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Seen At</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(sizeof($reports)> 0)
                            @foreach($reports as $u => $row)
                                <tr>
                                    <td class="py-1">
                                        @if($row->reportable_type)
                                            @php
                                                $arr = explode('\\', $row->reportable_type);
                                            @endphp
                                            {{ end($arr) }}
                                        @endif
                                    </td>

                                    <td>
                                        {{ strip_tags(\Illuminate\Support\Str::words($row->reportable->description, 30)) }}
                                    </td>
                                    <td>{{ $row->owner ? $row->owner->name : $row->admin->name }}</td>

                                    <td>{{ $row->reporter->name }}</td>
                                    @if($row->status == 1)
                                        <td><label class="badge badge-success">Seen</label></td>
                                    @elseif($row->status == 0)
                                        <td><label class="badge badge-warning">Unseen</label></td>
                                    @endif
                                    <td>{{\Carbon\Carbon::parse($row->created_at)->format('d-m-Y')}}</td>
                                    <td>{{ $row->seen_at ? \Carbon\Carbon::parse($row->seen_at)->format('d-m-Y') : ''}}</td>
                                    <td>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
