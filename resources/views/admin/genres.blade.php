@extends('admin.layouts.main')

@section('title')
    Genres
@endsection

@section('content')


    <button type="button" class="btn btn-primary btn-sm float-right m-3" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" id="addNewBtn"> Add New </button>
    <div class="col-lg-12 grid-margin stretch-card collapse" id="collapseExample">
        <div class="card">
            <div class="card-header">
                Add Genre
            </div>
            <div class="card-body">
                <form id="addNewGenreForm" action="{{route('admin.genres.store')}}" method="POST">
                    @csrf
                    <label>Genre Name</label>
                    <input type="text" name="genres" class="form-control genres" >
                    <input type="button" name="add_genre" class="btn btn-primary btn-sm mt-3 float-right" id="submitGenreBtn" value="Submit">
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Genres Table</h4>
                {{--                <p class="card-description">--}}
                {{--                    Add class <code>.table</code>--}}
                {{--                </p>--}}
                <div class="table-responsive">
                    <table class="table table-striped selfDataTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Genre Name</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(sizeof($genres)> 0)
                            @foreach($genres as $t => $t_row)
                                <tr>
                                    <td class="py-1">
                                        {{$t+1}}
                                    </td>

                                    <td>{{@$t_row->genres}}</td>
                                    <td>{{\Carbon\Carbon::parse(@$t_row->created_at)->format('d-m-Y')}}</td>
                                    <td><a href="{{route('admin.genres.destroy',['uuid'=>$t_row->uuid])}}"><span class="mdi mdi-delete" style="color: red">Delete</span></a></td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
<script type="text/javascript">
    $(document).on('click','#submitGenreBtn',function (e){
        e.preventDefault();
       let genreName = $('.genres').val();
       if (genreName != ''){

           $('#addNewGenreForm').submit();
       }else{
           alert('Please Enter Genre Name');
       }

    })
</script>

@endpush
