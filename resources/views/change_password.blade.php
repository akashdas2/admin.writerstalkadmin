
@extends('layouts.main')
@section('title')
    Change Password
@endsection
@section('content')
    <div class="w-12">
    </div>
    <div class="w-13">
        <div class="card w-100 border-0 bg-white shadow-xs p-0 mb-4 mt-2">
            <div class="card-body p-4 w-100 bg-primary border-0 d-flex rounded-3 ">
                <a href="{{route('userSetting')}}" class="d-inline-block mt-2"><i class="fas fa-arrow-left font-sm text-white"></i></a>
                <h4 class="font-xs text-white fw-600 ms-4 mb-0 mt-1">Change Password</h4>
            </div>
            <div class="card-body p-lg-5 p-4 w-100 border-0">
                <form action="#">
                    <div class="row">
                        <div class="col-lg-12 mb-3">
                            <div class="form-gorup">
                                <label class="mont-font fw-600 font-xssss">Current Password</label>
                                <input type="text" name="comment-name" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-12 mb-3">
                            <div class="form-gorup">
                                <label class="mont-font fw-600 font-xssss">Change Password</label>
                                <input type="text" name="comment-name" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 mb-3">
                            <div class="form-gorup">
                                <label class="mont-font fw-600 font-xssss">Confirm Change Password</label>
                                <input type="text" name="comment-name" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 mb-0">
                            <a href="#" class="bg-primary text-center text-white font-xsss fw-600 p-3 w175 rounded-3 d-inline-block">Save</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="w-14">
    </div>
@endsection
@push('js')

@endpush
