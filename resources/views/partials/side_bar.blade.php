 <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
 <aside class="left-sidebar" data-sidebarbg="skin6">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- User Profile-->
                <li class="sidebar-item pt-2">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('home')}}"
                       aria-expanded="false">
                        <i class="far fa-clock" aria-hidden="true"></i>
                        <span class="hide-menu">Dashboard</span>
                    </a>
                </li>

                <!--List Products-->
                <li class="sidebar-item pt-2">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('get-products')}}"
                       aria-expanded="false">
                        <i class="far fa-plus-square" aria-hidden="true"></i>
                        <span class="hide-menu">List Products</span>
                    </a>
                </li>
                <!-- Product Add-->
               
                <li class="sidebar-item pt-2">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('add-product')}}"
                       aria-expanded="false">
                        <i class="far fa-plus-square" aria-hidden="true"></i>
                        <span class="hide-menu">Add Basic Product</span>
                    </a>
                </li>
            
                <!-- Add product Pro -->
                 <li class="sidebar-item pt-2">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('pro')}}"
                       aria-expanded="false">
                        <i class="far fa-plus-square" aria-hidden="true"></i>
                        <span class="hide-menu">Add Pro Product</span>
                    </a>
                </li>
                <!-- Event Manager-->
                <li class="sidebar-item pt-2">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('event-manager')}}"
                       aria-expanded="false">
                        <i class="fas fa-indent" aria-hidden="true"></i>
                        <span class="hide-menu">Event Manager</span>
                    </a>
                </li>


                <li class="sidebar-item pt-2">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('get-translated-text')}}"
                       aria-expanded="false">
                        <i class="fas fa-indent" aria-hidden="true"></i>
                        <span class="hide-menu">List Translated Text</span>
                    </a>
                </li>

                <li class="sidebar-item pt-2">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('create-text')}}"
                       aria-expanded="false">
                        <i class="far fa-plus-square" aria-hidden="true"></i>
                        <span class="hide-menu">Add Translate Text</span>
                    </a>
                </li>
                <!-- Logout-->
                <li class="sidebar-item pt-2">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('logout') }}"
                       aria-expanded="false" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="fas fa-sign-out-alt" aria-hidden="true"></i>
                        <span class="hide-menu"> {{ __('Logout') }}</span>
                    </a>


                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </li>
                {{--<li class="sidebar-item">--}}
                    {{--<a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('home')}}"--}}
                       {{--aria-expanded="false">--}}
                        {{--<i class="fa fa-user" aria-hidden="true"></i>--}}
                        {{--<span class="hide-menu">Profile</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="sidebar-item">--}}
                    {{--<a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('home')}}"--}}
                       {{--aria-expanded="false">--}}
                        {{--<i class="fa fa-table" aria-hidden="true"></i>--}}
                        {{--<span class="hide-menu">Basic Table</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="sidebar-item">--}}
                    {{--<a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('home')}}"--}}
                       {{--aria-expanded="false">--}}
                        {{--<i class="fa fa-font" aria-hidden="true"></i>--}}
                        {{--<span class="hide-menu">Icon</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="sidebar-item">--}}
                    {{--<a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('home')}}"--}}
                       {{--aria-expanded="false">--}}
                        {{--<i class="fa fa-globe" aria-hidden="true"></i>--}}
                        {{--<span class="hide-menu">Google Map</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="sidebar-item">--}}
                    {{--<a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('home')}}"--}}
                       {{--aria-expanded="false">--}}
                        {{--<i class="fa fa-columns" aria-hidden="true"></i>--}}
                        {{--<span class="hide-menu">Blank Page</span>--}}
                    {{--</a>--}}
                {{--</li>--}}


            </ul>

        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
