<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="//www.grapevine.es/DE/wp-content/uploads/2020/09/facicon.png" type="image/x-icon" />
    <link rel="apple-touch-icon" href="//www.grapevine.es/DE/wp-content/uploads/2020/09/facicon.png">
    <link rel="apple-touch-icon" sizes="120x120" href="//www.grapevine.es/DE/wp-content/uploads/2020/09/facicon.png">
    <link rel="apple-touch-icon" sizes="76x76" href="//www.grapevine.es/DE/wp-content/uploads/2020/09/facicon.png">
    <link rel="apple-touch-icon" sizes="152x152" href="//www.grapevine.es/DE/wp-content/uploads/2020/09/facicon.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('template/plugins/images/favicon.png')}}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">

    <!-- Custom CSS -->
    <link href="{{asset('template/plugins/bower_components/chartist/dist/chartist.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('template/plugins/bower_components/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.css')}}">
    <!-- Custom CSS -->
    <link href="{{asset('template/css/style.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('template/css/custom.css')}}">
    @yield('styles')
</head>
