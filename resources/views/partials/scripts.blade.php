<!-- All Jquery -->
<!-- ============================================================== -->

<script src="{{asset('template/plugins/bower_components/jquery/dist/jquery.min.js')}}"></script>

<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('template/bootstrap/dist/js/bootstrap.bundle.min.js')}}">

<script src="{{asset('template/js/app-style-switcher.js')}}"></script>

<script src="{{asset('template/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js')}}"></script>

<!--Wave Effects -->
<script src="{{asset('template/js/waves.js')}}"></script>
<!--Menu sidebar -->
<script src="{{asset('template/js/sidebarmenu.js')}}"></script>
<!--Custom JavaScript -->
<script src="{{asset('template/js/custom.js')}}"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
<!--This page JavaScript -->
<!--chartis chart-->
<script src="{{asset('template/plugins/bower_components/chartist/dist/chartist.min.js')}}"></script>
<script src="{{asset('template/plugins/bower_components/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js')}}"></script>
<script src="{{asset('template/js/pages/dashboards/dashboard1.js')}}"></script>
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

