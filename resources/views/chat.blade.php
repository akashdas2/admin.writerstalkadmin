

@extends('layouts.main')
@section('title')
    Stories
@endsection
@section('content')
    <div class="w-22">
        <div class="row no-gutters pr-2">
            <div class="col-lg-12">
                <div class="chat-wrapper pt-0 mt-2 w-100 bg-white">
                    <div class="chat-body pt-3 px-3 pb-0">
                        <div class="messages-content">
                            <div class="message-item">
                                <div class="message-user">
                                    <figure class="avatar">
                                        <img src="assets/imgs/2.jpg" alt="image">
                                    </figure>
                                    <div>
                                        <h5>Byrom Guittet</h5>
                                        <div class="time">01:35 PM</div>
                                    </div>
                                </div>
                                <div class="w-50">
                                    <div class="message-wrap">I'm fine, how are you 😃</div>
                                </div>
                            </div>
                            <div class="message-item outgoing-message">
                                <div class="message-user">
                                    <figure class="avatar">
                                        <img src="assets/imgs/7.jpg" alt="image">
                                    </figure>
                                    <div>
                                        <h5>Byrom Guittet</h5>
                                        <div class="time">01:35 PM<i class="fas fa-check-double text-info"></i></div>
                                    </div>
                                </div>
                                <div class="w-100 w-right">
                                    <div class="message-content w-50 ml-auto">
                                        <div class="message-wrap ml-auto">I want those files for you. I want you to send 1 PDF and 1 image file.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="message-item">
                                <div class="message-user">
                                    <figure class="avatar">
                                        <img src="assets/imgs/2.jpg" alt="image">
                                    </figure>
                                    <div>
                                        <h5>Byrom Guittet</h5>
                                        <div class="time">01:35 PM</div>
                                    </div>
                                </div>
                                <div class="w-50">
                                    <div class="message-wrap">I've found some cool photos for our travel app.</div>
                                </div>
                            </div>
                            <div class="message-item outgoing-message">
                                <div class="message-user">
                                    <figure class="avatar">
                                        <img src="assets/imgs/7.jpg" alt="image">
                                    </figure>
                                    <div>
                                        <h5>Byrom Guittet</h5>
                                        <div class="time">01:35 PM<i class="fas fa-check-double text-info"></i></div>
                                    </div>
                                </div>
                                <div class="w-100 w-right">
                                    <div class="message-content w-50 ml-auto">
                                        <div class="message-wrap ml-auto">Hey mate! How are things going ? </div>
                                    </div>
                                </div>
                            </div>
                            <div class="message-item">
                                <div class="message-user">
                                    <figure class="avatar">
                                        <img src="assets/imgs/2.jpg" alt="image">
                                    </figure>
                                    <div>
                                        <h5>Byrom Guittet</h5>
                                        <div class="time">01:35 PM</div>
                                    </div>
                                </div>
                                <figure>
                                    <img src="assets/imgs/bb-9.jpg" class="w-25 img-fluid rounded-3" alt="image">
                                </figure>
                            </div>
                            <div class="message-item outgoing-message">
                                <div class="message-user">
                                    <figure class="avatar">
                                        <img src="assets/imgs/7.jpg" alt="image">
                                    </figure>
                                    <div>
                                        <h5>Byrom Guittet</h5>
                                        <div class="time">01:35 PM<i class="fas fa-check-double text-info"></i></div>
                                    </div>
                                </div>
                                <div class="w-100 w-right">
                                    <div class="message-content w-50 ml-auto">
                                        <div class="message-wrap ml-auto" style="margin-bottom: 90px;">Hey mate! How are things going ?</div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="chat-bottom p-3 shadow-none w-100" >
                    <form class="chat-form">
                        <button class="bg-grey float-left"><i class="fas fa-microphone-alt text-grey-600"></i></button>
                        <div class="form-group"><input type="text" placeholder="Start typing.."></div>
                        <button class="bg-current"><i class="fas fa-arrow-right text-white"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')

@endpush
