<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::post('register', [\App\Http\Controllers\ApiControllers\AuthController::class, 'register']);
Route::post('login', [\App\Http\Controllers\ApiControllers\AuthController::class, 'login']);

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('change-password', [\App\Http\Controllers\ApiControllers\UserController::class,'change_password']);
    Route::get('user', [\App\Http\Controllers\ApiControllers\AuthController::class, 'user']);
    Route::post('submit-user-post', [\App\Http\Controllers\ApiControllers\PostController::class, 'submit_user_post']);
    Route::post('get-user-posts', [\App\Http\Controllers\ApiControllers\PostController::class,'get_user_posts']);

    Route::post('submit-user-story', [\App\Http\Controllers\ApiControllers\PostController::class,'submit_user_story']);
    Route::get('get-user-stories', [\App\Http\Controllers\ApiControllers\PostController::class,'get_user_stories']);

    Route::post('submit-user-hightlight', [\App\Http\Controllers\ApiControllers\PostController::class,'submit_user_highlight']);
    Route::get('get-user-hightlights', [\App\Http\Controllers\ApiControllers\PostController::class,'get_user_hightlights']);



    Route::post('save-user-commment', [\App\Http\Controllers\ApiControllers\PostController::class,'save_user_comment']);
    Route::post('get-post-comments', [\App\Http\Controllers\ApiControllers\PostController::class,'get_post_comments']);
    Route::post('save-user-like', [\App\Http\Controllers\ApiControllers\PostController::class,'save_user_like']);

//    Route::post('get-admin-posts', [\App\Http\Controllers\ApiControllers\PostController::class,'get_admin_posts']);

    Route::post('update-profile', [\App\Http\Controllers\ApiControllers\UserController::class, 'update_profile']);

    Route::get('get-admin-posts', [\App\Http\Controllers\ApiControllers\PostController::class,'get_admin_posts']);


    Route::post('user-add-penpal',[\App\Http\Controllers\ApiControllers\UserController::class, 'user_add_penpal']);
    Route::post('user-profile-data',[\App\Http\Controllers\ApiControllers\UserController::class, 'user_profile_data']);

    Route::post('user-chat',[\App\Http\Controllers\ApiControllers\UserController::class, 'user_chat']);
    Route::get('get-user-chats',[\App\Http\Controllers\ApiControllers\UserController::class, 'get_user_chats']);
    Route::post('get-user-chat-messages',[\App\Http\Controllers\ApiControllers\UserController::class, 'get_user_chat_messages']);

    Route::post('create-group',[App\Http\Controllers\ApiControllers\GroupChatController::class,'create_group']);
    Route::get('get-user-groups',[App\Http\Controllers\ApiControllers\GroupChatController::class,'get_user_groups']);
    Route::post('store-message',[App\Http\Controllers\ApiControllers\GroupChatController::class,'store_message']);
    Route::post('get-group-messages',[App\Http\Controllers\ApiControllers\GroupChatController::class,'get_group_messages']);

    Route::get('get-user-favourites',[\App\Http\Controllers\ApiControllers\PostController::class, 'get_user_favourites']);
    Route::post('get-user-penpals', [\App\Http\Controllers\ApiControllers\UserController::class,'get_user_penpals']);
    Route::post('update-penpal-status', [\App\Http\Controllers\ApiControllers\UserController::class,'update_penpal_status']);
    Route::post('logout', [\App\Http\Controllers\ApiControllers\AuthController::class, 'logout']);
});
